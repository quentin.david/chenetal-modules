const express = require('express');
const morgan = require('morgan');
const moduleController = require('./controllers/moduleController');

const mysql = require('mysql');

// // Connect to morfetik DB
// let connection = mysql.createConnection({
//   host: process.env.DATABASE_HOST || 'localhost',
//   user: process.env.DATABASE_USERNAME || 'quentin',
//   password: process.env.DATABASE_PASSWORD || 'pass',
//   database: process.env.DATABASE_NAME || 'morfetik2'
// });

// connection.connect( (err) => {
//   if (err) throw err;
//   console.log('Connected to the Morfetik (simple form) database!')
// })

const port = process.env.API_PORT || 3005;

const app = express();

app.use(express.json());
app.use(morgan('dev'));

app.get('/', (req, res) => {
  res.send('Welcome to the morfetik (simple form) module')
});

app.post('/', moduleController.executeProcess);

app.listen(port, () => {
  console.log('Morfetik (simple form) module listening at http://localhost:'+port)
});
