/// *****
/// XXX module helper
/// *****

const { exec } = require('child_process');
const path = require('path');
const fs = require('fs');
const mysql = require('mysql2/promise');
const { CLIENT_RENEG_WINDOW } = require('tls');

// Execute a command given a String and the path it is supposed to be launched from
function executeCommand(command, workingPath) {
  return new Promise( (resolve, reject) => {
    exec(command, { cwd: workingPath}, (error, stdout, stderr) => {
      if (error) {
        reject(error.message);
        return;
      }
      else if (stderr) {
        reject(stderr);
        return;
      }
      else {
        resolve(stdout);
      }
    });
  });
}
exports.executeCommand = executeCommand;

// Parse parameters into a dict
exports.parseParameters = function(reqParameters) {
  let parameters = {};
  reqParameters.forEach( parameter => {
    parameters[parameter.name] = parameter.value;
  });
  return parameters;
}

exports.executeModule = async function(input, parameters) {

  try {
    let conlluColumns = {};
    parameters['selectedColumns'].forEach(column => {
      conlluColumns[column] = '';
    });

    let connection = await mysql.createConnection({
      host: 'localhost',
      user:'quentin',
      password:'pass',
      database:'morfetik2'
    });

    const lines = input.split('\n');
    let outputData = '';
    let queryResult;
    let queryString;

    for (var i = 0; i < lines.length - 1; i++) {

    //lines.forEach(async line => {
      let line = lines[i];
      let [tokenIdx, form, lem, pos] = line.split('\t');
      if( form == undefined) {
        console.log(line);
      }
      form = form.toLowerCase(); // If it's the beginning of a sentence, it will be capitalized

      // It's a metadata or a jumpline, no information to be added
      if(/^#/.test(tokenIdx) || tokenIdx == '') {
        Object.keys(conlluColumns).map( colName => conlluColumns[colName] += '\n');
      }
      else {
        // Maybe a more optimised version would be to perform a single query
        // to fetch all the informations at once, rather than performing a lot of queries.
        if (catGram = checkAndGetCorrectPOS(pos, parameters['acceptedCategories'])) {
          queryString = getQueryString(form, lem, catGram, parameters['selectedColumns']);
          //queryResult = await connection.query(queryString)
          queryResult = await queryWrapper(queryString, connection);

          // We want to get info on that token
          conlluColumns = formatAndUpdateConlluColumns(queryResult, conlluColumns);
        }
        // No info needed for that token, let's put '_'
        else {
          Object.keys(conlluColumns).map( colName => conlluColumns[colName] += '_\n');
        }
      }
    };
    connection.end();

    return formatConlluColsOutput(conlluColumns)
}
  catch (error) {
    throw error;
  }
}

// Build the queryString from the token line & the required columns
function getQueryString(form, lem, catGram, requiredColumns) {
  const correspondantColumns = {
    'Sous-Catégorie':'souscatgram',
    'Temps':'temps',
    'Nombre':'num',
    'Genre':'genre',
    'Personne':'person',
    'Notes':'notes'
    //Domaine (pas encore implémenté, il faut chercher dans les autres tables)
  }
  // Build selected columns, and removes last comma
  let selectedColumns = '';
  requiredColumns.forEach(el => {
    selectedColumns += correspondantColumns[el] + ','
  })
  selectedColumns = selectedColumns.slice(0,-1);

  // If there are quotes (") present in the word, escape them
  form = form.replace(/"/g,'\\"');
  lem = lem.replace(/"/g,'\\"');

  let queryString = `SELECT ${selectedColumns} FROM formes WHERE forme IN(\"${form}\",\"${form}°\") AND lemme=\"${lem}\" AND catgram LIKE \"${catGram}\";`;
  return queryString;
}

// Query the rows and return an object with the list of distincts values found
async function queryWrapper(queryString, connection) {
  try {
    const [rows, fields] = await connection.query(queryString);
    
    // Fuse all rows into a single object
    // (side-effect: fuse same values)
    let returnedValue = {};
    rows.forEach( row => {
      Object.keys(row).map( colName => {
        if ( !returnedValue[colName]) {
          returnedValue[colName] = [];
        }
        // Add to the list if: not null, not empty string & not already in the list.
        if (row[colName] != undefined && row[colName] != '' && (returnedValue[colName].indexOf(row[colName]) == -1)) {
          returnedValue[colName].push(row[colName]);
        }
      });
    });
    return returnedValue;
  }
  catch(error) {
    console.error('There was an issue with the following query:\n'+queryString);
    throw Error(error);
  }
}

// Updates the conllu columns given the object corresponding to the query
function formatAndUpdateConlluColumns(queryResult, conlluColumns) {
  let correspondance = {
    // 'Sous-catégorie' : 'souscatgram',
    'Genre':'genre',
    'Notes':'notes',
    'Nombre':'num',
    'Personne':'person',
    'Temps':'temps'
  };
  Object.keys(conlluColumns).map( colName => {
    let colValue = queryResult[correspondance[colName]];
    if (colValue && colValue != undefined && colValue.length) {
      conlluColumns[colName] += colValue.join(';') + '\n';
    }
    else {
      conlluColumns[colName] += '*\n';
    }
  });
  return conlluColumns;
}

// Given a tree-tagger POS and a list of allowed categories,
// Return the correct form of the POS for Morfetik DB if it is allowed
// Else, return null
function checkAndGetCorrectPOS(treeTaggerPos, allowedCategories) {
  const notAllowedCategories = ['PUN','PUN:cit','SENT','NUM','NAM','ABR']

  if (notAllowedCategories.indexOf(treeTaggerPos) != -1) {
    return null;
  }
  else if (treeTaggerPos.match(/^NOM/)) { // should we include proper nouns ? ( /NOM|NAM/)
    // nm|nf
    return (allowedCategories.indexOf('Nom') != -1) ? 'n_' : null;
  }
  else if (treeTaggerPos.match(/^VER:*/)) {
    return (allowedCategories.indexOf('Verbe') != -1) ? 'vrb' : null;
  }
  else if (treeTaggerPos.match(/^ADJ/)) {
    return (allowedCategories.indexOf('Adjectif') != -1) ? 'adj%' : null;
  }
  else if (treeTaggerPos.match(/^ADV/)) {
    return (allowedCategories.indexOf('Adverbe') != -1) ? 'adv': null;
  }
  else if (treeTaggerPos.match(/^KON/)) {
    return (allowedCategories.indexOf('Conjonction') != -1) ? 'C:%' : null;
  }
  else if (treeTaggerPos.match(/^DET:*/)) {
    return (allowedCategories.indexOf('Déterminant') != -1) ? 'D:%' : null;
  }
  else if (treeTaggerPos.match(/^PRO:*/)) {
    return (allowedCategories.indexOf('Pronom') != -1) ? 'P:%' : null;
  }
  else if (treeTaggerPos.match(/^PRP*/)) {
    return (allowedCategories.indexOf('Préposition') != -1) ? 'Prép' : null;
  }
  else if (treeTaggerPos.match(/^INT/)) {
    return (allowedCategories.indexOf('Interjection') != -1) ? 'Interj' : null;
  }
  else {
    //UNHANDLED CASES
    throw Error('Unhandled cases in checkAndGetCorrectPOS: '+treeTaggerPos );
  }
  // const morfetikCatgram = {
  //   'Nom': 'n*',
  //   'Verbe':'vrb',
  //   'Adj':'adj*',
  //   'Adv':'adv',
  //   'Conjonction':'C:*',
  //   'Déterminant':'D:*',
  //   'Pronom':'P:*',
  //   'Prépositiont':'Prép',
  //   'Interjection':'Interj'
  // }
  /*
  Nom: NAM, NOM
  Verbe: VER:
  Adj: ADJ
  Adv: ADV
  COnjonction: KON
  Déterminant: DET:ART
  Pronom: PRO:PER, PRO:IND, PRO:REL
  Préposition: PRP, PRP:det
  Interjection: INT
  */
}

// Transform this object to a list of conllu models
function formatConlluColsOutput(conlluColumns) {
  let outputList = []
  Object.keys(conlluColumns).map( colName => {
    outputList.push({
      columnTitle: 'Morfetik:'+colName.toUpperCase(),
      columnData: conlluColumns[colName]
    })
  });
  return outputList;
}
