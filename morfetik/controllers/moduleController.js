// ***
// Morfetik (simple form) module controller
// ***

const axios = require('axios');

const { errorHandler, successHandler } = require('./lib/responseHelper.js');
const moduleHelper = require('./lib/moduleHelper.js');

const apiURL = process.env.API_URL || 'http://localhost:8081';
const exporterURL = 'http://localhost:3001';

exports.executeProcess = async function(req, res) {

  const corpusProcessId = req.body.corpusProcessId;
  console.log(`Starting Morfetik process for corpusProcess id: ${corpusProcessId}`);
  const parameters = moduleHelper.parseParameters(req.body.parameters);

  try {
    // Fetch columns the module needs
    const exporterResponse = await axios.post(`${exporterURL}/columns`,
      {
        corpusProcessId: corpusProcessId,
        columns: ["tokensIdx","tokens", "lemmas", "POStags"],
        // Default: no unknown: false & shouldIncludeMetadata: true
        // We might consider what to do when the LEM is unknown
        // My actual solution: put noUnknwon so the LEM is the FORM
        options: {noUnknown: true}
      }
    );
    const moduleInput = exporterResponse.data.data.selectedColumnsString;

    // Process the data and possibly get the conllu col and/or the output string
    const conlluCols = await moduleHelper.executeModule(moduleInput, parameters);

    // Send the results to the backend
    for(let conlluColIdx in conlluCols) {
      let conlluCol = conlluCols[conlluColIdx];
      await axios.post(`${apiURL}/corpusProcesses/${corpusProcessId}/process`, { conlluCol});
    };
    console.log('Done with processing of '+corpusProcessId);
    successHandler(res, { message: 'Successfully applied Morfetik' });

    // Let's continue the pipeline
    axios.post(`${apiURL}/corpusProcesses/${corpusProcessId}/continue`);
  }
  catch(error) {
    errorHandler(res, error.message);
  }
}
