# Module morfetik


# Morfetik - Forme simple

On envoie chaque token à un dictionnaire (base SQL) qui nous renvoie un tagging plus précis

L'utilisateur choisit les parties du discours dont il veut les informations
Puis il choisit les informations supplémentaires qu'il veut récupérer.

Param1: liste des POS qui nous intéresse (tout le reste ne sera pas traité)
Nom, Verbe, Adj, Adv, 
Grammaire (Conjonction, Det, Pronom, Préposition, Interjection)

Param2: Requetes
Sous-catégorie, Temps, Nombre, Genre, Personne, Notes, -Domaine- (cf https://tal.lipn.univ-paris13.fr/morfetik/search/search)

## Output

Morfetik ajoute une colonne par colonnes sélectionnées dans le deuxième paramètre. Ceux-ci auront pour titre : MORFETIK:GENRE par exemple.

Chaque token aura alors 3 valeurs possibles :

- '_' : Cela signifie que ce token a été ignoré, aucune information n'a été cherchée
- '*' : Cela signifie que ce token a été cherché, mais qu'aucune information n'a été trouvé par cette colonne particulièrement
- x;y;z : Cela signifie que ce token a été cherché et qu'une ou plusieurs informations ont été trouvées. Il est en effet possible d'avoir plusieurs informations en même temps sur une même colonne car on ne peut pas désambiguiser (par exemple 'vis' (vivre) peut-être vivre à la première ou seconde personne du présent de l'indicatif, ou bien peut-être la seconde personne du présent de l'indicatif ou du présent de l'impératif). Dans ce cas là, on trouvera toutes les informations possibles séparées par un point-virgule _;_ 

## TODO / Next features

### Domaine

Pour l'instant, la catégorie _Domaine_ n'est pas intégrée à Morfetik

### Connexion à la base du serveur TAL

Plutôt que de récupérer un dump de la base SQL Morfetik, il pourrait être intéressant de se connecter directement à la base afin d'être toujours à jour quant à ses futures ajouts & modifications.


## Notes
Demander à Aude où trouver l'information 'Domaine'
-> Présente par exemple dans nslemmes (select * from nslemmes where lemme='angioblaste' limit 10;)

Nom, Verbe, Adj, Adv
-> tables formes colonne 'cat'

Grammaire:
colonne 'catgram'
Conjonction: commence par C:
Det: commence par D:
Pronom: commence par P:
Préposition: Prép
Interjection: Interj



INPUT:
forme + lemme + POS

SELECT colonne1, colonne2,... from forms where forme = token_forme, lemme = token_lemme, cat = token_POS

Attention, dans forme il peut y avoir mot° qui signifie que ce motest une forme rare. Plutôt faire 'LIKE' mot°?$ (vérifier comment faire une regex en sql)

OUTPUT:

Deux possibilités:

1 seule colonne conllu Morfetik

ou

n colonnes conllu Morfetik

Une seule colonne:
sous-cat:x,y|num:1,2|....

Plusieurs colonnes:

Morfetik:Sous-catégorie
x,y

Morfetik:Nombre
1,2


TODO:

- Accéder à la BDD
- Récupérer la table forme
- Créer les requêtes SQL à partir d'un token
- Idéalement: accéder directement à la table Morfetik2 du serveur TAL
