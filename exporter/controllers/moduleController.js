// ***
// Exporter module controller
// ***

const axios = require('axios');
const path = require('path');
const fs = require('fs');

const { errorHandler, successHandler } = require('./lib/responseHelper.js');
const exporterHelper = require('./lib/exporterHelper.js');

const apiURL = process.env.API_URL || 'http://localhost:8081';

// Fetch the documents and POS/LEMMAS/FORMS
// And create and export a conllu file.
exports.executeProcess = async function(req, res) {
  const corpusProcessId = req.body.corpusProcessId;

  try {
    const corpusProcessRes = await axios.get(`${apiURL}/corpusProcesses/${corpusProcessId}`);
    const corpusId = corpusProcessRes.data.data.corpusProcess.corpusId;
    const conlluCols = corpusProcessRes.data.data.corpusProcess.conlluCols;

    const corpusResponse = await axios.get(`${apiURL}/corpora/${corpusId}`);
    const documents = corpusResponse.data.data.corpus.documents;
    
    exportConllu(documents, conlluCols, corpusProcessId);
    exportProcessOutputs(corpusProcessRes.data.data.corpusProcess.processOutputs, corpusProcessId);

    console.log("All the documents have been successfully exported to conllu");
    successHandler(res, { message: "All the documents have been successfully exported to conllu"});

  }
  catch (error) {
    errorHandler(res, error);
  }
}

// Given a list of columns and a corpusProcessId, return a string of those columns
exports.getStringFromSelectedColumns = async function (req, res) {
  const corpusProcessId = req.body.corpusProcessId;
  const selectedColumns = req.body.columns;
  // Options: shouldIncludeMetadata (default: true), noUnknown (default false)
  const options = req.body.options;

  try {
    const corpusProcessRes = await axios.get(`${apiURL}/corpusProcesses/${corpusProcessId}`);
    const processColumns = corpusProcessRes.data.data.corpusProcess.conlluCols;
    const corpusId = corpusProcessRes.data.data.corpusProcess.corpusId;

    const corpusResponse = await axios.get(`${apiURL}/corpora/${corpusId}`);
    const documents = corpusResponse.data.data.corpus.documents;

    const selectedColumnsString = exporterHelper.getSelectedColumns(documents, processColumns, selectedColumns, options);

    // Maybe we should add a boolean to ask whether store it or not.
    let exportDirectory = path.join(__dirname, '/../exportFiles', corpusProcessId);
    if (!fs.existsSync(exportDirectory)) {
      fs.mkdirSync(exportDirectory);
    }

    let exportedFile = path.join(exportDirectory, '1' + '.conllu');
    fs.writeFile(exportedFile, selectedColumnsString, (err => {
      if (err) throw Error(err);
    }));

    successHandler(res, { selectedColumnsString });
  }
  catch (error) {
    console.log(error);
    errorHandler(res, error.message);
  }
};

exports.readConlluAndProcessOutputs = async function (req,res) {
  let returnData = {}
  try {
    const corpusProcessId = req.params.corpusProcessId;

    const corpusProcessRes = await axios.get(`${apiURL}/corpusProcesses/${corpusProcessId}`);
    const corpusProcess = corpusProcessRes.data.data.corpusProcess;
    const corpusId = corpusProcess.corpusId;
    const conlluCols = corpusProcess.conlluCols;

    const corpusResponse = await axios.get(`${apiURL}/corpora/${corpusId}`);
    const documents = corpusResponse.data.data.corpus.documents;

    returnData['conllu'] = exporterHelper.convertDocumentsToConllu(documents, conlluCols);

    // If there is some process outputs, return it
    if(corpusProcess.processOutputs) {
      returnData['processOutputs'] = corpusProcess.processOutputs;
    }
    successHandler(res, returnData);
  }
  catch(error) {
    console.log(error);
    errorHandler(res, error.message);
  }
}

function exportConllu(documents, conlluCols, corpusProcessId) {
  const conlluString = exporterHelper.convertDocumentsToConllu(documents, conlluCols);

  let exportDirectory = path.join(__dirname, '/../exportFiles', corpusProcessId);
  if (!fs.existsSync(exportDirectory)) {
    fs.mkdirSync(exportDirectory);
  }

  let exportedFile = path.join(exportDirectory, '1' + '.conllu');
  fs.writeFile(exportedFile, conlluString, (err => {
    if (err) throw Error(err);
  }));
}

function exportProcessOutputs(processOutputs, corpusProcessId) {

  let exportDirectory = path.join(__dirname, '/../exportFiles', corpusProcessId);
  // Export conllu have been called before, it should be created, I will still be checking if that's the case
  if (!fs.existsSync(exportDirectory)) {
    // Should trow an error? if we goes here it means conllu have not been saved
    throw Error('Exporter: exportProcessOutputs, directory has not been created, conllu probably were not saved.');
    //fs.mkdirSync(exportDirectory);
  }
  processOutputs.forEach( processOutput => {
    // Maybe the name should be changed, or the extension or the file.
    let fileName = processOutput.moduleName + '_' + processOutput._id + '.txt';
    let exportedFile = path.join(exportDirectory, fileName);
    fs.writeFile(exportedFile, processOutput.content.data, (err => {
      if(err) throw Error(err);
    }));
  });

}