exports.errorHandler = function(res, error, httpCode = 500) {
    res.status(httpCode);
    res.json({
        status: "error",
        message: error
    });
    console.error(error);
}
exports.successHandler = function(res, data, httpCode = 200) {
    res.status(httpCode);
    res.json({
        status: "success",
        data: data
    });
}