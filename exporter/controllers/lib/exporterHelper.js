function convertDocumentToConllu(document) {
  let newString = "";
  const { tokensIdx, tokens, lemmas, POStags } = document;

  const tokensIdxList = tokensIdx.columnData.split('\n');
  const tokensList = tokens.columnData.split('\n');
  const lemmasList = lemmas.columnData.split('\n');
  const POStagsList = POStags.columnData.split('\n');

  // check equality of strings 
  if (
    tokensList.length !== lemmasList.length || 
    lemmasList.length !== POStagsList.length ||
    POStagsList.length !== tokensIdxList.length
    ) {
    throw Error('Difference of length in POS or lemmas or tokens for '+ document._id);
  }
  // ID\tFORM\tLEMMA\tUPOS\n 
  for (var i = 0; i < tokensList.length; i++) {
    newString += [tokensIdxList[i], tokensList[i], lemmasList[i], POStagsList[i]].join('\t') + '\n';
  }
  return newString;
}

exports.convertDocumentsToConllu = function (documents, conlluCols ) {
  let conlluColsSplitted = []
  conlluCols.forEach( conlluCol => {
    conlluColsSplitted.push({ columnTitle: conlluCol.columnTitle, columnData: conlluCol.columnData.split('\n')});
  });
  let newString = "";

  documents.forEach(document => {

    const { tokensIdx, tokens, lemmas, POStags } = document;

    const tokensIdxList = tokensIdx.columnData.split('\n');
    const tokensList = tokens.columnData.split('\n');
    const lemmasList = lemmas.columnData.split('\n');
    const POStagsList = POStags.columnData.split('\n');

    // check equality of strings 
    if (
      tokensList.length !== lemmasList.length || 
      lemmasList.length !== POStagsList.length ||
      POStagsList.length !== tokensIdxList.length
      ) {
      throw Error('Difference of length in POS or lemmas or tokens for '+ document._id);
    }
    // ID\tFORM\tLEMMA\tUPOS\n 
    for (var i = 0; i < tokensList.length; i++) {
      let cols = [tokensIdxList[i], tokensList[i], lemmasList[i], POStagsList[i]];
      conlluColsSplitted.forEach( col => {
        cols.push(col.columnData.shift());
      })
      newString += cols.join('\t') + '\n';
    }
  });
  return newString;
}

exports.convertToConllu = function(documents, conlluCols) {
  let l = [];
  documents.forEach( document => {
    let { conllu, conlluColsConsumed } = convertDocumentToConllu(document, conlluColsSplitted)
    l.push({ conllu: convertDocumentToConllu(document), id: document._id});
  });
  return l;
}


exports.getSelectedColumns = function (documents, processColumns, selectedColumns, options ) {
  let { shouldIncludeMetadata, noUnknown } = options;

  // Setting defaults 
  if (!shouldIncludeMetadata) { shouldIncludeMetadata = true; }
  // no unknown for lemmas
  if (!noUnknown) { noUnknown = false; }

  // User asks for un unknown, so we switch all LEMMAS selected col for LEMMAS_NO_UNKNOWN
  if (noUnknown) {
    selectedColumns = selectedColumns.map( col => {
      return col === 'lemmas' ? 'lemmas_no_unknown' : col;
    });
  }

  // Build tokens index to iterate the rows and skip metadata
  let tokensIdxColumn = '';
  documents.forEach( document => {
    tokensIdxColumn += document.tokensIdx.columnData;
  });
  tokensIdxColumn = tokensIdxColumn.split('\n');

  // Build a list with splitted selected columns
  let selectedColumnConlluList = [];
  selectedColumns.forEach( selectedColumn => {
    if (  selectedColumn == 'POStags'|| 
          selectedColumn == 'lemmas' || 
          selectedColumn == 'lemmas_no_unknown' ||
          selectedColumn == 'tokens' || 
          selectedColumn == 'tokensIdx') {
      let buffer = '';
      documents.forEach( document => {
        buffer += document[selectedColumn].columnData
      })
      selectedColumnConlluList.push(buffer.split('\n'));
    }
    else {
      selectedColumnConlluList.push(processColumns[selectedColumn].columnData);
    }
  });

  let outputString = '';
  for (let i = 0; i < tokensIdxColumn.length; i++) {
    // If current token is a metadata and that the boolean is false, pass
    if (tokensIdxColumn[i][0] == '#' && !shouldIncludeMetadata) {
      continue
    }
    else {
      outputString += selectedColumnConlluList.map( column => column[i]).join('\t') + '\n';
    }
  }
  return outputString;
}

