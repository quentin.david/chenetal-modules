const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const moduleController = require('./controllers/moduleController');

const port = process.env.API_PORT || 3001;

const app = express();
app.use(express.json());
app.use(morgan('dev'));
app.use(cors());


app.get('/', (req, res) => {
  res.send('Welcome to the exporter module')
});
// Fetch all columns and build a conllu file
app.post('/', moduleController.executeProcess);
// Fetch only selected columns - Options: shouldIncludeMetadata (default: true), noUnknown (default false)
app.post('/columns', moduleController.getStringFromSelectedColumns);
// Return data for the files given a finished corpus process
app.get('/get-files/:corpusProcessId', moduleController.readConlluAndProcessOutputs);




app.listen(port, () => {
  console.log('Exporter module listening at http://localhost:'+port)
});
