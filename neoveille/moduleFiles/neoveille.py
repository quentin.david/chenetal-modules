import argparse
from unidecode import unidecode
import re

import lib.excluded_dico_client as excluded_dico_client
import lib.hunspell_client as hunspell_client

def read_candidate_files(filepath):
  candidate_list = []
  with open(filepath) as f:
    for _,candidate in enumerate(f):
      candidate_list.append(candidate.strip())
  return candidate_list

# Default pattern: \w{3,}(?:-\w{3,}){0,3}
def pattern_wrapper(candidate_list, user_pattern, nodigit, lowercaseonly):
  def match_pattern(word,compiled_regex):
    word = unidecode(word.strip().replace('-', ''))
    return compiled_regex.match(word) and (not(lowercaseonly) or word.islower()) or (not(nodigit) and re.search(r'/d', word))
  
  if (user_pattern == ''):
    user_pattern = r'.*'
  compiled_regex = re.compile(user_pattern)
  return [candidate for candidate in candidate_list if match_pattern(candidate, compiled_regex)]

  
def exclusion_dict_wrapper(candidate_list, excluded_dico_server_url):
  # Return true if the word is absent from the exclusion dict
  def is_not_in_exclusion_dict(word):
    word = unidecode(word.strip().replace('-', ''))
    LANG_ISO = "fr"
    return not(excluded_dico_client.get_entry(excluded_dico_server_url, word, LANG_ISO))
  return [candidate for candidate in candidate_list if is_not_in_exclusion_dict(candidate)]

def hunspell_heuristic_wrapper(candidate_list):
  def does_pass_hunspell_heuristic(word):
    #main_dict = ling_config['hunspell']['main_dict']
    main_dict= '/home/qdavid/Travail/neoveille2020/ling_resources/hunspell-dicos/france/fr_FR'
    (res, _) = hunspell_client.hunspell_check_word(word, main_dict)
    return res
  return [candidate for candidate in candidate_list if does_pass_hunspell_heuristic(candidate)]

if __name__ == '__main__':
  debug = False

  parser = argparse.ArgumentParser(description='Executable python script for Neoveille Module for CheneTAL')

  parser.add_argument('--pattern', action="store", help='A regex to check for every neologism candidate')
  parser.add_argument('--nodigit', action="store_true", help='Exclude candidates if there is digits in their characters')
  parser.add_argument('--lowercaseonly', action="store_true", help='Exclude candidates that are not lower case')
  parser.add_argument('--exclusiondict', action='store_true', help='Exclude candidates if they are present in the exclusion dictionary')
  parser.add_argument('--spellingcheck', action='store_true', help='Exclude candidates under some heuristics using hunspell')
  parser.add_argument('-i', '--input', action='store', required=True, help='Path of the file of the candidates, one candidate per line')
  args = parser.parse_args()

  candidate_list = read_candidate_files(args.input)
  
  if debug:
    print("Regex: {}".format(args.pattern))
    print('Arguments: \nnogit:{}, lowercase:{}, spellingcheck:{}, exclusiondict:{}'.format(args.nodigit, args.lowercaseonly, args.spellingcheck, args.exclusiondict))
    print(candidate_list)
  #candidate_list = ['bonjour', 'byebye', 'shorté', 'macronisé', 'bouzillé', 'covided','covidosceptique']
  

  if args.pattern:
    candidate_list = pattern_wrapper(candidate_list, args.pattern, args.nodigit, args.lowercaseonly)
  if args.spellingcheck:
    candidate_list = hunspell_heuristic_wrapper(candidate_list)
  if args.exclusiondict:
    excluded_dico_server_url = 'http://127.0.0.1:5056'
    candidate_list = exclusion_dict_wrapper(candidate_list,excluded_dico_server_url)
  

  print("\n".join(candidate_list))
