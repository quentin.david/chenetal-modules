# Neoveille module for CheneTAL

This is an implementation of [Néoveille](https://tal.lipn.univ-paris13.fr/neoveille/html/) for CheneTAL.

## Install dependencies

`npm install`

`pip install unidecode`


## How to run

`npm run start`

Will launch the web server on default port _3004_.

It can then receive API calls from the backend server of CheneTAL.

If you are using exclusion dict, you must configurate and then launch _excluded_dico_server.py_ in moduleFiles/lib:

`python3 moduleFiles/lib/excluded_dico_server.py fr it pl cz`
