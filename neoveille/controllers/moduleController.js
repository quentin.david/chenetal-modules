// ***
// Neoveille module controller
// ***

const axios = require('axios');

const { errorHandler, successHandler } = require('./lib/responseHelper.js');
const moduleHelper = require('./lib/moduleHelper.js');

const apiURL = process.env.API_URL || 'http://localhost:8081';
const exporterURL = 'http://localhost:3001'

exports.executeProcess = async function(req, res) {
  const corpusProcessId = req.body.corpusProcessId;
  let parameters = req.body.parameters;
  try {
    parameters = moduleHelper.parseParameters(parameters);
    const exporterResponse = await axios.post(`${exporterURL}/columns`,
      {
        corpusProcessId: corpusProcessId,
        columns: ["tokensIdx", "tokens", "lemmas"],
        options: { noUnknown: false, shouldIncludeMetadata: true}
      }
    );
    const neoveilleInput = exporterResponse.data.data.selectedColumnsString;
    const conlluCol = await moduleHelper.executeNeoveille(neoveilleInput, parameters);

    await axios.post(`${apiURL}/corpusProcesses/${corpusProcessId}/process`, { conlluCol});
    successHandler(res, 'Neoveille success');

    // Let's continue the pipeline
    axios.post(`${apiURL}/corpusProcesses/${corpusProcessId}/continue`);
  }
  catch (error) {
    errorHandler(res, error.message);
  }
}
