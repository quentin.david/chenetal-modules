/// ******
/// Neoveille module helper
/// ******

const { exec } = require('child_process');
const path = require('path')
const fs = require('fs');

function executeCommand(command, workingPath) {
  return new Promise( (resolve, reject) => {
    exec(command, { cwd: workingPath}, (error, stdout, stderr) => {
      if (error) {
        reject(error.message);
        return;
      }
      else if (stderr) {
        reject(stderr);
        return;
      }
      else {
        resolve(stdout);
      }
    });
  });
}
exports.executeCommand = executeCommand;

exports.parseParameters = function(reqParameters) {
  let parameters = {};
  reqParameters.forEach( parameter => {
    parameters[parameter.name] = parameter.value;
  });
  return parameters;
}

exports.executeNeoveille = async function(input, parameters) {
  // First, we build a list to candidates, ie that have been lemmed <unknown> by treetagger
  // But also a hashtable storing their offset
  const { candidateList, offsetHashTable } = parseInputForNeoveille(input);

  const candidateFilePath = saveCandidates(candidateList);

  let commandString = `python3 neoveille.py`
  if (parameters["pattern"]) commandString += ` --pattern '${parameters["pattern"]}'`;
  if (parameters["exclusiondict"]) commandString += ' --exclusiondict';
  if (parameters["spellingcheck"]) commandString += ' --spellingcheck';
  if (parameters["nodigit"]) commandString += ' --nodigit';
  if (parameters["lowercaseonly"]) commandString += ' --lowercaseonly';
  commandString += ` --input ${candidateFilePath}`
  const scriptWorkingPath = path.join(__dirname, '../../moduleFiles');
  try {
    let stdout = await executeCommand(commandString, scriptWorkingPath);
    let electedList = stdout.split('\n');
    return buildConlluCol(input, electedList, offsetHashTable);
  }
  catch(error) {
    throw Error(error);
  }
}

// Given idx and elected tokens list, build the conllu col
function buildConlluCol(input, electedList, hashTable) {
  const lines = input.split('\n');
  let outputData = '';
  let currentSentenceId = 0;

  lines.forEach( line => {
    let tokenIdx = line.split('\t')[0];

    // Metadata: add \n (and jump line when we encounter one)
    if (/^#/.test(tokenIdx)) {
      outputData += '\n';
      if(/^# sent_id =/.test(tokenIdx)) {
        currentSentenceId += 1;
      }
    }
    else {
      // token is both in the hash table & the elected list
      if ( hashTable[currentSentenceId] && (electedList.indexOf(hashTable[currentSentenceId][tokenIdx]) > -1)) {
        outputData += '+\n';
      }
      // It's a space
      else if (tokenIdx == '') {
          outputData += '\n';
        }
      // It's a token line but no neologisms detected here
      else {
        outputData += '_\n';
      }
    }
  })
  return {
    columnTitle: 'Neoveille',
    columnData: outputData
  };
}

// Given idx, token and lemmas, return a list of candidates (where lemma == <unknown>)
// But also a list of offset (dictionary of lines idx and tokens idx)
function parseInputForNeoveille(input) {
  const lines = input.split('\n');
  let candidateList = [];
  let offsetHashTable = {};
  let sentenceCount = 0

  lines.forEach( line => {
    let [idx, token, lemma] = line.split('\t');
    if (idx == 1) {
      sentenceCount += 1;
    }
    if (lemma == '<unknown>') {
      candidateList.push(token);
      if (!offsetHashTable[sentenceCount]) {
        offsetHashTable[sentenceCount] = { };
      }
      offsetHashTable[sentenceCount][idx] = token;
    }
  });
  return { candidateList, offsetHashTable };
}

// Save the input string into a temporary file called 1.conllu
function saveCandidates(inputArray) {
  let exportDirectory = path.join(__dirname, '/../../moduleFiles/temporaryFiles');
  if (!fs.existsSync(exportDirectory)) {
    fs.mkdirSync(exportDirectory);
  }

  let savedFilePath = path.join(exportDirectory, 'candidate_list');
  fs.writeFileSync(savedFilePath, inputArray.join('\n'));
  // fs.writeFile(savedFile, inputString, (err => {
  //   if (err) throw Error(err);
  // }));
  return savedFilePath;
}
