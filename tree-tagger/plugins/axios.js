const axios = require("axios");

exports.axios = axios.create({
  baseUrl: process.env.MODULES_URL || "http://localhost:3000"
  // headers: { Authorization: Bearer jwt_token }
});