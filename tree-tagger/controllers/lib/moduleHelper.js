const { exec } = require('child_process');

// NEED TO SANITIZE ????????
module.exports.executeCommand = function(command) {
  return new Promise( (resolve, reject) => {
    exec(command, (error, stdout, stderr) => {
      if (error) {
        reject(error);
        return;
      }
      else if (stderr) {
        reject(stderr);
        return;
      }
      else {
        resolve(stdout);
      }
    });
  });
}