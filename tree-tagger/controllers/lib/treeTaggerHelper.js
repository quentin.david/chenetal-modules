const { executeCommand } = require('./moduleHelper.js');
const path = require('path');
const fs = require('fs')
const treeTaggerPath = path.join(__dirname,'../../moduleFiles')


function parseTreeTaggerOutput(output, documentId) {
  let result = {tokensIdx: `# document_id = ${documentId}\n`, tokens: "\n", POStags: "\n", lemmas: "\n", lemmas_no_unknown: "\n"};
  let lines = output.trim().split('\n');
  let tokensIdx = 1;
  let currentSentence = "";
  let sentenceId = 0;
  let bufferString = { tokensIdx: "", tokens: "", POStags: "", lemmas: "", lemmas_no_unknown: ""};
  lines.forEach( line => {
    let row = line.split('\t');
    bufferString.tokensIdx += tokensIdx + "\n";
    bufferString.tokens += row[0] + "\n";
    bufferString.POStags += row[1] + "\n";
    bufferString.lemmas += row[2] + "\n";
    bufferString.lemmas_no_unknown += row[2] == '<unknown>' ? row[0] + "\n" : row[2] + "\n";
    currentSentence += " " + row[0];

    // End of sentence, new line, enter sent id & sent text, then reset sent idx
    if (row[1] === 'SENT') {
      result.tokensIdx += "# sent_id = " + sentenceId + "\n# text =" + currentSentence + "\n";
      result.tokensIdx += bufferString.tokensIdx + "\n";
      result.tokens += "\n\n" + bufferString.tokens + "\n";
      result.POStags += "\n\n" + bufferString.POStags + "\n";
      result.lemmas += "\n\n" + bufferString.lemmas + "\n";
      result.lemmas_no_unknown += "\n\n" + bufferString.lemmas_no_unknown + "\n";

      sentenceId += 1;

      // Reset
      tokensIdx = 1;
      currentSentence = "";
      bufferString = { tokensIdx: "", tokens: "", POStags: "", lemmas: "", lemmas_no_unknown: ""};
    }
    else {
      tokensIdx += 1;
    }
  });
  return result;
}

function getLanguageCommandPath(langCode) {
  const langDict = {
    'fr':'tree-tagger-french',
    'en':'tree-tagger-english',
    'de':'tree-tagger-german',
    'it':'tree-tagger-italian',
    'ru':'tree-tagger-russian',
    'sw':'tree-tagger-french'
  };
}

// Generally, if you are not expecting large amounts of data to be returned, you can use exec() for simplicity. 
// Good examples of use-cases are creating a folder or getting the status of a file. 
// However, if you are expecting a large amount of output from your command, then you should use spawn().
// A good example would be using command to manipulate binary data and then loading it in to your Node.js program.

// NEED TO SANITIZE ????????
let tokenizeDocument = async function(document) {
  // Escape spaces
  const filePath = " \"" + document.source + "\"" ;
  // -f for french, -a for abbreviation list
  const tokenizeCommand = path.join(treeTaggerPath, "/cmd/utf8-tokenize.perl") + " -f -a "
                        + path.join(treeTaggerPath+"/lib/french-abbreviations")
                        + filePath;
  try {
    const output = await executeCommand(tokenizeCommand);
    return output;
  }
  catch (error) {
    throw error;
  }
}

let tokenizeDocuments = async function(documents) {
  let l = []
  console.log(documents);
  for (const document of documents ) {
    try {
      let doc = await tokenizeDocument(document)
      l.push({_id: document._id, tokens: doc});
    }
    catch (error) {
      throw error;
    }
  }
  return l;
}



// TODO: change path based on language.
let runTreeTaggerOnDocument = async function(document) {
  //const filePath = "\"" + path.join(sourcePath + document.source) + "\"" ;
  const filePath = '"'+document.source+'"';
  // Weird: TreeTagger prints some outputs (verbose stuff) in stderr
  // So I suppress it with 2>/dev/null
  try {
    // const treeTaggerCommand = path.join(treeTaggerPath+"/cmd/tree-tagger-french") + " " + filePath + " 2> /dev/null";
    const treeTaggerCommand = path.join(treeTaggerPath,"/cmd/tree-tagger-french") + " " + filePath + " > ./moduleFiles/temporaryOutput/treeTagOutput 2> ./moduleFiles/temporaryOutput/error.log";
    await executeCommand(treeTaggerCommand);
    const output = fs.readFileSync( path.join(treeTaggerPath, "/temporaryOutput/treeTagOutput"), { encoding: 'utf8'} );
    return output;
  }
  catch (error) {
    throw Error("Error in runTreeTaggerOnDocument, check the issue with the treetagger program.");
  }
}

let runTreeTaggerOnDocuments2 = async function(documents) {
  let l = [];
  let outputString = "#global.columns = ID FORM ";
  for (const document of documents) {
    try {
      outputString += ""
      const processedDoc = await runTreeTaggerOnDocument(document);
      const parsedDoc = parseTreeTaggerOutput(processedDoc, document._id); 
       l.push({_id: document._id, ...parsedDoc});
      //l.push({_id: document._id, tokens: doc.tokens, POStags: doc.POStags, lemmas: doc.lemmas})
    }
    catch (error) {
      throw error;
    }
  }
  return l;
}

let runTreeTaggerOnDocuments = async function(documents) {
  let l = [];
  for (const document of documents) {
    try {
      
      const processedDoc = await runTreeTaggerOnDocument(document);
      const parsedDoc = parseTreeTaggerOutput(processedDoc,document._id);
      l.push({_id: document._id, ...parsedDoc});
      //l.push({_id: document._id, tokens: doc.tokens, POStags: doc.POStags, lemmas: doc.lemmas})
    }
    catch (error) {
      throw error;
    }
  }
  return l;
}

exports.runTreeTaggerOnDocuments = runTreeTaggerOnDocuments
exports.tokenizeDocuments = tokenizeDocuments;