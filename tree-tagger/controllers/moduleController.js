// ***
// TreeTagger module controller
// ***

//const { axios } = require('../plugins/axios.js');
const axios = require('axios')
//import axios from 'axios';
const { errorHandler, successHandler } = require('./lib/responseHelper');
const treeTaggerHelper = require('./lib/treeTaggerHelper.js');

const apiURL = process.env.API_URL || 'http://localhost:8081';


// NOTES:
// What should happens if some documents are sent but not others because of an issue?
// TODO: not do anything if already pos tagged
// Receive a corpusId, fetch the content
// Apply tree-tagger, get the result and send it back.
exports.executeProcess = async function(req, res) {
  const corpusProcessId = req.body.corpusProcessId;
  try {
    const corpusProcessResponse = await axios.get(`${apiURL}/corpusProcesses/${corpusProcessId}`);
    const corpusId = corpusProcessResponse.data.data.corpusProcess.corpusId;

    const corpusResponse = await axios.get(apiURL+'/corpora/'+corpusId);
    const corpus = corpusResponse.data.data.corpus;
    const documents = corpus.documents;
    const processedDocuments = await treeTaggerHelper.runTreeTaggerOnDocuments(documents);
    for (var i = 0; i < processedDocuments.length ; i++) {
      let processedDocument = processedDocuments[i];
      await axios.post(apiURL+'/corpora/'+corpusId+'/'+processedDocument._id, 
            { tokens: {
                columnTitle: 'FORM',
                columnData: processedDocument.tokens
              },
              lemmas: {
                columnTitle: 'LEMMA',
                columnData: processedDocument.lemmas
              },
              lemmas_no_unknown: {
                columnTitle: 'LEMMA_NO_UNKNOWN',
                columnData: processedDocument.lemmas_no_unknown
              },
              POStags: {
                columnTitle: 'UPOS',
                columnData: processedDocument.POStags
              },
              tokensIdx: {
                columnTitle: 'ID',
                columnData: processedDocument.tokensIdx
              }
            });
      console.log(`Processed document ${processedDocument._id} has been added!`);
    };
    successHandler(res, { message: "All the documents have been successfully processed with tree tagger"});
    
    // Let's continue the pipeline
    axios.post(`${apiURL}/corpusProcesses/${corpusProcessId}/continue`);
  }
  catch (error) {
    errorHandler(res, error);
  }
}