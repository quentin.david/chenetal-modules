// ***
// XXX module controller
// ***

const axios = require('axios');

const { errorHandler, successHandler } = require('./lib/responseHelper.js');
const moduleHelper = require('./lib/moduleHelper.js');

const apiURL = process.env.API_URL || 'http://localhost:8081';
const exporterURL = 'http://localhost:3001';

exports.executeProcess = async function(req, res) {

  const corpusProcessId = req.body.corpusProcessId;
  console.log(`Starting XXX process for corpusProcess id: ${corpusProcessId}`);
  const parameters = moduleHelper.parseParameters(req.body.parameters);

  try {
    // Fetch columns the module needs
    const exporterResponse = await axios.post(`${exporterURL}/columns`,
      {
        corpusProcessId: corpusProcessId,
        // This parameter ask for the name of the column in the struct if it's from treetagger
        // Else, the name of the column (columnTitle)
        // This is confusing and should be normalized to always be columnTitle
        // POS : POStags, form : tokens, ID: tokensIdx
        columns: [],
        // Options: shouldIncludeMetadata (default: true), noUnknown (default: false)
        options: {}
      }
    );
    const moduleInput = exporterResponse.data.data.selectedColumnsString;

    // Process the data and possibly get the conllu col and/or the output string
    const { processOutput, conlluCol } = moduleHelper.executeModule(moduleInput, parameters);

    // Send the results to the backend
    await axios.post(`${apiURL}/corpusProcesses/${corpusProcessId}/process`, { conlluCol, output});
    console.log('Done with processing of '+corpusProcessId);
    successHandler(res, { message: 'Successfully applied XXX' });

    // Let's continue the pipeline
    axios.post(`${apiURL}/corpusProcesses/${corpusProcessId}/continue`);
  }
  catch(error) {
    errorHandler(res, error.message);
  }
}
