exports.errorHandler = function(res, error, httpCode = 500) {
    res.status(httpCode);
    res.json({
        status: "error",
        message: error
    });
    console.error(error);
}
exports.successHandler = function(res, data, httpCode = 200) {
    res.status(httpCode);
    res.json({
        status: "success",
        data: data
    });
}
// For eg. "User not found" is a fail, not an error
exports.failHandler = function(res, data, httpCode) {
    res.status(httpCode);
    res.json({
        status: "fail",
        data: data
    });
}