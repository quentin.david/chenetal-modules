/// *****
/// XXX module helper
/// *****

const { exec } = require('child_process');
const path = require('path');
const fs = require('fs');

// Execute a command given a String and the path it is supposed to be launched from
function executeCommand(command, workingPath) {
  return new Promise( (resolve, reject) => {
    exec(command, { cwd: workingPath}, (error, stdout, stderr) => {
      if (error) {
        reject(error.message);
        return;
      }
      else if (stderr) {
        reject(stderr);
        return;
      }
      else {
        resolve(stdout);
      }
    });
  });
}
exports.executeCommand = executeCommand;

// Parse parameters into a dict
exports.parseParameters = function(reqParameters) {
  let parameters = {};
  reqParameters.forEach( parameter => {
    parameters[parameter.name] = parameter.value;
  });
  return parameters;
}

// This is where your module is processing the input
exports.executeModule = function (moduleInput, parameters) {
  try {
    // Fill here the program logic
    return { 
      processOutput: null,
      // conlluCol: { columnTitle: 'MODULE', columnData: 'outputData'}
      conlluCol: null    
    }

  }
  catch (error) {
    throw Error(error);
  }
}