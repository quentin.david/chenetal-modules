const express = require('express');
const morgan = require('morgan');
const moduleController = require('./controllers/moduleController');

const port = process.env.API_PORT || 3000;

const app = express();

app.use(express.json());
app.use(morgan('dev'));

app.get('/', (req, res) => {
  res.send('Welcome to the XXX module')
});

app.post('/', moduleController.executeProcess);

app.listen(port, () => {
  console.log('XXX module listening at http://localhost:'+port)
});
