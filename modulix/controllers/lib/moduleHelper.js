
// Form a string representing a conllu file from all the documents
exports.mergeDocumentsConllu = function(conlluList, shouldRemoveMetadata=false) {
  let outputString = ''
  conlluList.forEach(conlluObj => {
    outputString += convertDocumentToConllu(conlluObj, shouldRemoveMetadata);
  })
  return outputString;
}

exports.parseParameters = function(reqParameters) {
  let parameters = {};
  reqParameters.forEach( parameter => {
    parameters[parameter.name] = parameter.value;
  });
  return parameters;
}

function convertDocumentToConllu(document, shouldRemoveMetadata) {
  let newString = "";
  const { tokensIdx, tokens, lemmas, POStags } = document;

  const tokensIdxList = tokensIdx.columnData.split('\n');
  const tokensList = tokens.columnData.split('\n');
  const lemmasList = lemmas.columnData.split('\n');
  const POStagsList = POStags.columnData.split('\n');

  // check equality of strings 
  if (
    tokensList.length !== lemmasList.length || 
    lemmasList.length !== POStagsList.length ||
    POStagsList.length !== tokensIdxList.length
    ) {
    throw Error('Difference of length in POS or lemmas or tokens for '+ document._id);
  }
  // ID\tFORM\tLEMMA\tUPOS\n 
  for (var i = 0; i < tokensList.length; i++) {
    // Don't add anything is it is a metadata and the option is true
    if (shouldRemoveMetadata && tokensIdxList[i][0] === '#') {
      continue;
    }
    newString += [tokensIdxList[i], tokensList[i], lemmasList[i], POStagsList[i]].join('\t') + '\n';
  }
  return newString;
}

/// Modulix specific functions

// Create fake treatment: row of empty values ( '*' )
exports.modulixProcess = function(conllu, parameters ) {
  const conlluList = conllu.split('\n');
  const symbol = parameters.symbol;
  
  let processedString = '';
  
  conlluList.forEach( row => {
    // If row is a metadata or a line return delimiting a sentence ends.
    if (row[0] === '#' || row.slice(0,1) === '\t') {
      processedString += '\n';
    }
    else {
      processedString += symbol + '\n';
    }
  });
  const modulixConlluCol = {
    columnTitle: 'MODULIX',
    columnData: processedString
  };
  const modulixOutput = {
    moduleName: 'Modulix',
    content: {
      title: 'Output title',
      description: 'Output description with symbol:' + {'_':'Underscore', '*': 'Asterisk'}[symbol], 
      data: 'Output text of Modulix'
    }
  }

  return { conlluCol: modulixConlluCol, output: modulixOutput };
}