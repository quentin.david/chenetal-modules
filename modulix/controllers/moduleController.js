// ***
// Modulix module controller
// ***

// Modulix is a mockup module that fakes what a module should do.
// It fetches the corpus, does iterate through the rows then creates a new row with empty values

// * = empty values
// _ = underspecified annotations

const axios = require('axios');
const moduleHelper = require('./lib/moduleHelper.js');

const { errorHandler, successHandler } = require('./lib/responseHelper.js');

const apiURL = process.env.API_URL || 'http://localhost:8081';

exports.executeProcess = async function (req, res) {
  const corpusProcessId = req.body.corpusProcessId;
  const parameters = moduleHelper.parseParameters(req.body.parameters);

  try {
    // **** This following section can be done with exporter

    // Fetch documents
    const corpusProcessResponse = await axios.get(`${apiURL}/corpusProcesses/${corpusProcessId}`);
    const corpusId = corpusProcessResponse.data.data.corpusProcess.corpusId;

    const documentsConlluListResponse = await axios.get(`${apiURL}/corpora/${corpusId}/documents`);
    const documentsConlluList = documentsConlluListResponse.data.data.documentsConlluList;
    
    // Merge all documents into one conllu. The second argument is whether you want to remove metadata (I don't know if that can be useful...)
    const conllu = moduleHelper.mergeDocumentsConllu(documentsConlluList, false);
    //const conllu_off = moduleHelper.mergeDocumentsConllu(documentsConlluList, true);
    
    // ****

    // Process the module, fetch the conllu col & possibly the output.
    const { conlluCol, output } = moduleHelper.modulixProcess(conllu, parameters);
    
    // SHOULD WE SEND IT FROM HERE OR FROM THE BACK DIRECTLY?
    await axios.post(`${apiURL}/corpusProcesses/${corpusProcessId}/process`, { conlluCol, output});

    // Send the result back 
    successHandler(res, { message: 'Successfully applied modulix' });
  }
  catch (error) {
    errorHandler(res, error)
  }
}