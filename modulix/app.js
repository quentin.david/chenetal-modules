const express = require('express');
const moduleController = require('./controllers/moduleController');

const port = process.env.API_PORT || 3002;

const app = express();
app.use(express.json());

app.get('/', (req, res) => {
  res.send('Welcome to the modulix module')
});

app.post('/', moduleController.executeProcess);

app.listen(port, () => {
  console.log('Modulix module listening at http://localhost:'+port)
});
