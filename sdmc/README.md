# Module SDMC

## Installation

`npm install`

## Start the module

`npm run start`

## Outputs

### ProcessOutputs

The module returns an output listing all the different patterns that were found, along with the number of times they appear.

Here is an example of that output.
```
{avoir}	sup=394
{que}{de}	sup=132
{,}{et}	sup=263
{,}{,}{et}	sup=133
{,}{et}{le}	sup=134
{le}{,}{et}	sup=136
{le}{,}{le}	sup=267
```
### conlluCol

If the token is not part of a pattern, it will be annotated '_'.
Otherwise, it will be annotated _x_:_y_, where _x_ is the index of the pattern (you can find the corresponding one in the processOutput), and _y_ is the index of the token in that pattern, starting from 0.

__10:2__ would then means : the third (we start at zero!) token in the tenth pattern.

A token can be part of multiple patterns at once. In that case, informations from each of the patterns will be separated with a semi-colon.

__3:0;5:1__ would then means : this token is BOTH the first token of the the third pattern AND the second token of the fifth pattern.

## TODOS

- Rewrite moduleController to have something similar to Prototype module. (not very important, but improve consistency)
- Modify pearl script so that it can matches each time the same pattern is found (right now, if a pattern is present multiple times, only the first time is matched). This fix might need to rewrite the function _buildConlluCol_.
- If the user has selected categories (categories parameter) but chose LEM or FORM (motif_type parameter), the process shall run as the categories is set as _all_ (categories only apply on CAT and CAT+LEM)
- If the user has deselected all categories, the process shall run as if the user selected all the categories.

## Some notes on modification history

### TreeTag <unknown>:
The treetag is missing some information, notably unknown words. For example, in the resumé of Dune, 
Treetag tags the words 'Fremen', 'Bene' 'Generit' as <unknown> when using SDMC with LEM

- This issue have been solved by adding the `-no-unknown` parameter on treetagger module.

### Punctuation avoidance

Punctuation were avoided during the script. (see line :51 of PROC/RewriteTag.pl)
The script was changed so that patterns with punctuations are allowed.

The software might not be able to catch multiple times the same pattern in a single sentence.

### Multiple pattern match in a sentence (not solved)
We might want to test that the software catches two patterns in a sentence. If that is not the case, we could change the line
`if ($ligne ~= m/$regex/p)` by `while ($ligne ~= m/regex/p)` (/p seems to be deprecated btw)

### Changes in script_toXML.pl:

Changes have been made to script_toXML.pl because of an update of perl. These are the changes I've made to the script:

223 & 252:
$HashMotifs{$pos_first}}
->
%{$HashMotifs{$pos_first}}

### Changes in SequentialLinguists.sh

- Removal of php commands lines
- L.114: rm ../$TMPDIR/$CORPUS_TAG/TIME, TIME is never created

### Main launch script

This is the main script that needto be launched to retrieve all the necessary files.

If you only need the list of motifs and the number of occurrences, just launch sdmc.sh.

SequentialLinguist.sh

corpusPath
outputFolderName
lang (0=en, 1=fr)
tool (nom exe & nom répertoire) : 
	- BIDESpanTree ( Fermés, sans sous-motifs de même fréq )
	- GapBIDE ( ???? )
	- MaxSpanTree ( Maximaux, sans sous-motifs de même fréq )
	- PrefixConstrait (fréquents, tous les motifs)
choix (0 forme, 1 pos, 2 lemme, 3 pos+lem)
mail
format (0: texte brut, 1: tree-tagger, 2: cordial, 3: latin)
corpusName (used to send the mail, useless for chenetal)
gapmin
gapmax
nbitemsetmin
nbitemsetmax
minsup
constraint:
	0 : No constraints= Name, Verb, Adj, Adv
	1 : Name
	2 : Verb
	3 : Adj
	4 : Adv

1 2 3 means: both Name, Verb and Adjective.


Output files:

- *Treetag_data*: treetagged corpus
- *correspondance*: idx of each token (form/lem/pos, depending of your choice)
- *Dmt4_data*: abstract representation of the corpus.
seqId for the id of the sentence
Then the number of the token in the sentence, and its corresponding hash number from correspondance file.
- *resultat_0*: Results of all the motifs found - list of hashes and occurrences of the motif.

### Concordancer

script_concordancier.pl takes a specific motif as a parameters and fetch all the sentences in which it appears.
We will tweak this script a little to fetch the indexes of the motifs.

script_concordancier.pl

$uid: nameOfOutputFolder (go fetch it in out/ )
$file: resultat_0
$nl: indexDuToken (i-ème ligne du fichier resultat_0)
$corpus: Dmt4_Data
gapmin
gapmax
$decal: not used in the script

Génére le fichier concordancier.

### Treatment of concordancier

The number of lines informs on the total number of occurrences of a pattern.
For each line:
LINE_IDX TABULATION SENTENCE

You can then split the sentence and check for <b>WORD</b> and get the index which would be the offset in the sentence LINE_IDX.


