const express = require('express');
const morgan = require('morgan');
const moduleController = require('./controllers/moduleController');

const port = process.env.API_PORT || 3003;

const app = express();

app.use(express.json());
app.use(morgan('dev'));

app.get('/', (req, res) => {
  res.send('Welcome to the SDMC module')
});

app.post('/', moduleController.executeProcess);

app.listen(port, () => {
  console.log('SDMC module listening at http://localhost:'+port)
});
