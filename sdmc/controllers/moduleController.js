// ***
// SDMC module controller
// ***

const axios = require('axios');

const { errorHandler, successHandler } = require('./lib/responseHelper.js');
const moduleHelper = require('./lib/moduleHelper.js');

const apiURL = process.env.API_URL || 'http://localhost:8081';
const exporterURL = 'http://localhost:3001';

exports.executeProcess = async function(req, res) {
  const corpusProcessId = req.body.corpusProcessId;
  console.log(`Starting SDMC process for corpusProcess ${corpusProcessId}`);
  const parameters = moduleHelper.parseParameters(req.body.parameters);
  try {
        
    const exporterResponse = await axios.post(`${exporterURL}/columns`, 
      {
        corpusProcessId: corpusProcessId,
        columns: ["tokens", "POStags", "lemmas"],
        options: { noUnknown: true }
      }
    );
    // You get a string corresponding to a treetagger output
    const sdmcInput = exporterResponse.data.data.selectedColumnsString;
    const { processOutput, hashTable } = await moduleHelper.executeSDMC(sdmcInput, parameters);

    // Fetch tokensIdx col
    const secondExporterResponse = await axios.post(`${exporterURL}/columns`,
    {
      corpusProcessId: corpusProcessId,
      columns: ["tokensIdx","POStags"],
      options: {  shouldIncludeMetadata: true }
    });
    const selectedCols = secondExporterResponse.data.data.selectedColumnsString;
    const conlluCol = moduleHelper.buildConlluCol(selectedCols, hashTable);
    await axios.post(`${apiURL}/corpusProcesses/${corpusProcessId}/process`, { conlluCol, processOutput});

    console.log('Done with processing of '+corpusProcessId);
    successHandler(res, { message: 'Successfully applied SDMC' });

    // Let's continue the pipeline
    axios.post(`${apiURL}/corpusProcesses/${corpusProcessId}/continue`);
  }
  catch (error) {
    errorHandler(res, error.message);
  }
}
