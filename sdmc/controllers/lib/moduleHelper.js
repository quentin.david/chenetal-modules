/// *****
/// SDMC module helper
/// *****

const { exec } = require('child_process');
const path = require('path');
const fs = require('fs');

// NEED TO SANITIZE ????????
function executeCommand(command, workingPath) {
  return new Promise( (resolve, reject) => {
    exec(command, { cwd: workingPath}, (error, stdout, stderr) => {
      if (error) {
        reject(error.message);
        return;
      }
      else if (stderr) {
        reject(stderr);
        return;
      }
      else {
        resolve(stdout);
      }
    });
  });
}
exports.executeCommand = executeCommand;

exports.parseParameters = function(reqParameters) {
  let parameters = {};
  reqParameters.forEach( parameter => {
    parameters[parameter.name] = parameter.value;
  });
  return parameters;
}

exports.executeSDMC = async function(inputString, parameters) {
  try {
    // Save the input string into a file and get its path
    const savedFilePath = saveString(inputString);
    const outputFolder = 'temporaryOutputFolder';
    // Input is already treetagged so supposedly, we don't care about the corpus language (?)
    // 0=en, 1=fr
    const language = 0;
    const tool = {"Fréquents (tous les motifs)": 'PrefixConstraint', 
                  "Fermés (sans sous-motifs de même fréquence)": 'BIDESpanTree', 
                  "Maximaux (en ôtant les sous-motifs)": 'MaxSpanTree'}
                  [ parameters["motif_representation"]];
    // BAD NAME! But I don't know what I should call this parameter
    // (Représentation condensée des motifs on the module...)
    const choix = {"Forme du mot seul": 0, 
                  "Lemme du mot seul": 2, 
                  "Catégorie syntaxique du mot seul": 1,
                  "Catégorie syntaxique et lemme du mot": 3}
                  [ parameters["motif_type"]];

    // We don't use these so we put something useless
    const mail = 'isaac.asimov@foundation.org';
    const corpusName = 'Seldon'

    // 0: raw text, 1: tree-tagger, 2: cordial, 3: latin
    const format = 1;

    // ******** CONSTRAINTS **********
    // ONLY FOR Catégorie syntaxique du mot seul & Catégorie syntaxique et  lemme du mot
    // IF NOT THESE TWO, WE SHOULD WRITE 0
    // 0: No constraints (name+verb+adj+adv)
    // 1: name
    // 2: verb
    // 3: adj
    // 4: adv
    // '1 3 4' = name + adj + adv
    const constraint = parseConstraint(parameters["categories"], choix);

    // Because of perl locale warnings, I redirect stderr to dev null...
    // Bad practice!
    const commandString = `./sh/sequentialLinguists.sh ${savedFilePath} ${outputFolder} ${language}\
                          ${tool} ${choix} ${mail} ${format} ${corpusName}\
                          ${parameters["minimal_gap"]} ${parameters["maximal_gap"]}\
                          ${parameters["minimal_size"]} ${parameters["maximal_size"]}\
                          ${parameters["minimal_absolute_support"]} ${constraint} > logs/sequential_linguist_log 2>logs/sequential_linguist_error_log`
    
    // Set the path the script should be executed in
    const scriptWorkingPath = path.join(__dirname, '../../moduleFiles');
    await executeCommand(commandString, scriptWorkingPath);

    // Now get the hash table from the patterns
    // HashTable[LineIdx][tokenIdx] = [patternNumber_i:pattern_i_nth_word, patternNumber_j:pattern_j_kth_word]
    // This will be used to fill the conllu col
    const patternsHashTable = await executeConc(parameters);

    const processOutput = {
      moduleName: 'SDMC',
      content: {
        title: 'SDMC Results',
        description: 'List of all patterns found, along with their total count',
        data: readRawOutput(outputFolder)
      }
    };

    return { processOutput: processOutput, hashTable: patternsHashTable};
  }
  catch (error) {
    throw Error(error);
  }
}

// Execute the script concordancier for each of the patterns given the file resultat_0
// Then returns a hash table with every patterns:
// HashTable[LineIdx][tokenIdx] = [patternNumber_i:pattern_i_nth_word, patternNumber_j:pattern_j_kth_word]
async function executeConc(parameters) {
  const outputFolder = 'temporaryOutputFolder';
  //const outputFolder = 'petitprince';
  const inputFile = 'resultat_0';
  const corpus = 'Dmt4_data';
  const scriptWorkingPath = path.join(__dirname, `../../moduleFiles/`);

  try {
    // Get number of lines of the file, ie number of patterns found
    const patternFilePath = path.join(scriptWorkingPath,  `out/${outputFolder}/resultat_0 | awk '{print $1}'` );
    const commandString = `wc -l ${patternFilePath}`;

    let stdout = await executeCommand(commandString);
    const patternTotalCount = parseInt(stdout);
    //const patternTotalCount = 1;

    // If no patterns are found, we should do something special
    if (patternTotalCount == 0 ) {
      return undefined;
    }
    else {
      let patternsHashTable = {};
      // Generate a file for each of patterns found
      for (var patternIdx = 0 ; patternIdx < patternTotalCount; patternIdx++) {
        // Warnings are removed because it emits to stderr and causing issues.
        // I couldn'tfix the issue
        const commandString = `LANG=en_GB.UTF-8 ./script_concordancier.pl ${outputFolder} ${inputFile} ${patternIdx} ${corpus} ${parameters["minimal_size"]} ${parameters["maximal_size"]}`;
        await executeCommand(commandString,scriptWorkingPath);

        // File created is 'concordancier', we need to read it and transform it
        patternsHashTable = concordancierFileProcessing(outputFolder, patternIdx, patternsHashTable);
      }
      return patternsHashTable;
    }
  }
  catch (error) {
    throw Error(error);
  }
}


// 0: No constraints (name+verb+adj+adv)
// 1: name
// 2: verb
// 3: adj
// 4: adv
// '1 3 4' = name + adj + adv
function parseConstraint(categoriesList, choice) {
  // choix : {"Forme du mot seul": 0, 
  // "Lemme du mot seul": 2, 
  // "Catégorie syntaxique du mot seul": 1,
  // "Catégorie syntaxique et lemme du mot": 3}
  // If user has not selected a process with syntax categories, we decided
  // the process should not care whether the user has selected or deselected some categories
  // Just output 0.
  if (choice == 0 || choice == 2) { return 0; }
  let constraintString = '';
  let constraintDict = {
    'Nom': 1,
    'Verbe': 2,
    'Adjectif': 3,
    'Adverbe': 4
  };
  categoriesList.forEach(category => {
    constraintString += constraintDict[category] + ' ';
  });
  // If no categories were selected, then it's is similar to every categories have been added
  if (constraintString == '') { constraintString = '0 '};
  return constraintString;
}

// Save the input string into a temporary file called 1.conllu
function saveString(inputString) {
  let exportDirectory = path.join(__dirname, '/../../moduleFiles/temporaryFiles', "temp");
  if (!fs.existsSync(exportDirectory)) {
    fs.mkdirSync(exportDirectory);
  }

  let savedFilePath = path.join(exportDirectory, '1' + '.conllu');
  fs.writeFileSync(savedFilePath, inputString);
  // fs.writeFile(savedFile, inputString, (err => {
  //   if (err) throw Error(err);
  // }));
  return savedFilePath;
}

// Read a concordancier file and fill the hash table with the patterns
function concordancierFileProcessing(outputFolder, patternIdx, hashTable) {
  try {
    // Add one so it appears to start at 1 instead of 0
    patternIdx += 1;
    const filePath = path.join(__dirname, `../../moduleFiles/out/${outputFolder}/concordancier`);
    const concordancierData = fs.readFileSync( filePath, { encoding: 'utf8'} );
  
    // console.log(`\n\n\nPattern number: ${patternIdx}:\n${concordancierData}`);
    const lines = concordancierData.trim().split('\n');

    lines.forEach( line => {
      const lineSplit = line.split('\t');
      subProcess(lineSplit[0], lineSplit[1].trim().split(/\s+/));
    });

    return hashTable;
  }
  catch (error) {
    throw Error(error);
  }

  // Process each line to add it on the hash table
  function subProcess(lineIdx, tokenList) {
    // Count at which element of the pattern we're at
    // CAREFUL: SCRIPT_CONCORDANCER DOES NOT CHECK IF THERE ARE MULTIPLE
    // MATCHES IN A SINGLE SENTENCE, IF THAT WILL BE THE CASE, WE WILL NEED TO MODIFY THIS FUNCTION
    // (We should be aware of the length of the pattern at the beginning of the function)
    let patternTokenIdx = 0;
    // Validation regex for tokens that are in a pattern
    const regex = /<b>.*<\/b>/;

    for (var tokenIdx = 1; tokenIdx <= tokenList.length; tokenIdx++) {
      // For treetagger, first token is 1st, not 0 like in this array.
      let currentToken = tokenList[tokenIdx-1];
      if (regex.test(currentToken)) {
        // Fill the hash table accordingly to its state
        if (hashTable[lineIdx]) {
          if (hashTable[lineIdx][tokenIdx]) {
            hashTable[lineIdx][tokenIdx].push(`${patternIdx}:${patternTokenIdx}`);
          }
          else {
            hashTable[lineIdx][tokenIdx] = [`${patternIdx}:${patternTokenIdx}`];
          }
        }
        else {
          hashTable[lineIdx] = {};
          hashTable[lineIdx][tokenIdx] = [`${patternIdx}:${patternTokenIdx}`]
        }

        // Moving on the next pattern token!
        patternTokenIdx += 1;
      }
      // This token was not a pattern, let's continue with the next one
    }
    // Each token have been examined
  }
}

// Return string of resultat_correspondance from the output folder
function readRawOutput(outputFolder) {
  const filePath = path.join(__dirname, `../../moduleFiles/out/${outputFolder}/resultat_correspondance`);
  const fileData = fs.readFileSync( filePath, { encoding: 'utf8'});
  if (fileData == 'Le fichier est vide.\n') { return 'No pattern was found'}
  else { return fileData }
}

// Given the tokensIdx col and a hashtable of all the patterns
// Build the conllu col corresponding to SDMC output
exports.buildConlluCol = function (tokensIdxAndPOSCols, hashTable, noPunctuations = false) {
  try {

    const colsList = tokensIdxAndPOSCols.split('\n');
    // SDMC Conllu col to be built
    let outputColData = '';

    let currentSentenceId = 0;

    // Count the number of punctuation so that it ajusts to the index in concordancier
    let punctuationCount = 0;

    colsList.forEach( line => {
      let splitLine = line.split('\t');
      let tokenIdx = splitLine[0];
      let POS = splitLine[1];

      // Adjust for fetching in the hashtable
      // Is NaN when tokenIdx is a metadata...
      let adjustedToken = tokenIdx - punctuationCount;

      if (hashTable == undefined) { outputColData += '\n'}
      else {
        // Check if it is a metadata
        if (/^#/.test(tokenIdx)) {
          outputColData += '\n';

          // Sent_id is minus 1 the sentences marked in the correspondance file.
          if (/^# sent_id =/.test(tokenIdx)) {
            currentSentenceId += 1;
            // New sentence, so we reset the punctuation count
            punctuationCount = 0;
          }
        }
        // We're iterating tokens now !
        else {
          // If there is no punctuations included and that current token
          // is a punctuation, let's continue and adjust our count
          if( noPunctuations && POS == 'PUN') {
            // SHOULD BE CHANGED TO MATCH ^PUN
            outputColData += '_\n';
            punctuationCount += 1;
          }
          // A pattern have been found! Let's fill this row!
          else if (hashTable[currentSentenceId] && hashTable[currentSentenceId][adjustedToken]) {
            outputColData += hashTable[currentSentenceId][adjustedToken].join(';') + '\n';
          }
          // If no patterns for that line in the hashTable, just fill it with \n
          // Until we go to a line with patterns
          else {
            outputColData += '_\n';
          }
        }
      }
    })
    return {
      columnTitle: 'SDMCPatterns',
      columnData: outputColData
    };
  }
  catch (error) {
    throw Error(error);
  }
}