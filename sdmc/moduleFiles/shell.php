<?php
//var_dump($_SERVER);

require("/var/www/sdmc/web/config/config.php");

switch($_SERVER["argv"][1]) {
	case "add" :
		if(isset($_SERVER["argv"][2])) {
			$pid = $_SERVER["argv"][2];
			if(isset($_SERVER["argv"][3])) {
				$userMail = $_SERVER["argv"][3];
				$user = $userDB->getUser($userMail);		
				$name = $_SERVER["argv"][4];
                $corpusname = $_SERVER["argv"][5];
                $tool = $_SERVER["argv"][6];
                $binaire = "Inconnu";
                if ($tool == "BIDESpanTree"){
                    $binaire = "Fermés";
                }
                if ($tool == "MaxSpanTree"){
                    $binaire = "Maximaux";
                }
                if ($tool == "PrefixConstraint"){
                    $binaire = "Fréquents";
                }
                if ($tool == "GapBIDE"){
                    $binaire = "GapBIDE";
                }
                $gapmin = $_SERVER["argv"][7];
                $gapmax = $_SERVER["argv"][8];
                $nbitemsetmin = $_SERVER["argv"][9];
                $nbitemsetmax = $_SERVER["argv"][10];
                $minsup = $_SERVER["argv"][11];
                
				$process = new Process(array(
					"pid" => $pid,
					"user" => $user->getId(),
					"name" => $name,
                    "corpusname" => $corpusname,
                    "binaire" => $binaire,
                    "gapmin" => $gapmin,
                    "gapmax" => $gapmax,
                    "nbitemsetmin" => $nbitemsetmin,
                    "nbitemsetmax" => $nbitemsetmax,
                    "minsup" => $minsup   
				));
				$process->setDate(new DateTime());
				$processDB->addProcess($process); 	    
                
				echo "Le traitement a bien été ajouté. \n";
			}
			else {
				echo "Impossible d'ajouter le traitement en base : Pas d'id utilisateur. \n";
			}
		}
		else {
			echo "Impossible d'ajouter le traitement en base : Pas de PID. \n";
		}
	break;
	
	case "remove" :
		if(isset($_SERVER["argv"][2])) {
			$processDB->processOver($_SERVER["argv"][2]);			
			echo "Le traitement a bien été marqué comme terminé. \n";
		} else {
			echo "Pas de pid (pour supprimer) \n"; 
		}		
	break;
	
	case "mail" :
		if(isset($_SERVER["argv"][3])) {
			$mailer->processOver($_SERVER["argv"][3], $_SERVER["argv"][2]);
			file_put_contents("test_server.txt", $_SERVER["argv"][4]);
			//passthru("script_concordancier_toXML.pl  > /dev/null 2> logs/errorProcess &");
		}
	break;
}



?>
