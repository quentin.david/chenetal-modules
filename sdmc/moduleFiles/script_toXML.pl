#!/usr/bin/perl -w

#use strict;
use warnings;
use utf8;
use POSIX qw(strftime); 

my ($file,$corpus,$gapmin,$gapmax,$namecorpus) = @ARGV;
my $date = strftime "%Y/%m/%d", localtime; 

open (IN, $file) or die("Erreur à l'ouverture du fichier '$file'");
open (OUT, ">resu.xml") or die("Erreur à l'ouverture du fichier 'concordancier'");
open (Correspondance, "correspondance") or die("Erreur à l'ouverture du fichier 'correspondance'");
binmode(STDOUT, ':utf8');
binmode(STDERR, ':utf8');



#on contruit la hashmap de correspondance ID = mot
my %Correspondance;
while (<Correspondance>){
    chomp $_;
    my @Correspondance = split (/\t/, $_);
    $Correspondance{$Correspondance[1]} = $Correspondance[0];
}
close(Correspondance) or die("Erreur à la fermeture");




#on compte le nombre de mots dans le texte précédant chaque phrase pour utilisation ultérieure
my @TabMots;
my $nbMotsDepuisDebut = 0;
my $nbMots = 0;
open (CORPUS, $corpus) or die("Erreur à l'ouverture du fichier '$corpus'");
while (<CORPUS>){
	chomp $_;

	if($_ =~ m/seqId/){
		$nbMotsDepuisDebut += $nbMots;
		push(@TabMots, $nbMotsDepuisDebut);
		#print "$nbMotsDepuisDebut\n";
	}else{
		my @LigneCourante = split (/\s/, $_); #on ne peut pas juste ++, il faut faire attention aux itemsets (i.e. ne pas les compter deux fois)
        $nbMots = $LigneCourante[0];
	}	
}		
$nbMotsDepuisDebut += $nbMots;
push(@TabMots, $nbMotsDepuisDebut); #derniere ligne
close(CORPUS) or die("Erreur à la fermeture");



#on contruit la hashmap des motifs $HashMotifs{rangPremierMot}[]
my %HashMotifs;
#on contruit la hashmap des chaine $HashChaine{motif}[]
my %HashChaine;

#Pour chaque motifs en sortie de SDMC
my $cpt_motif = 0;
while (<IN>){

	chomp $_; #{18} {2} {6} : 390
	print "$_\n";
	
	my @Res = split (/\:/, $_);#$Res[0] = "{18} {2} {6}",$Res[1] = "390"
	my @Motif = split (/\s/, $Res[0]);#$Motif[0] = "{18}", $Motif[1] = "{2}", $Motif[2] = "{6}"
	
	my $regex = "";
	
	for (my $i = 0 ; $i <= $#Motif-1 ; $i++){        	
		if ($Motif[$i] =~ m/\{+\S+\}/){
			$Motif[$i] =~ s/\{//;
			$Motif[$i] =~ s/\}//;
			
			my @item = split(/,/, $Motif[$i]);    
			if(@item == 2){
				$regex .= " ($item[0],$item[1])(?: (?:[0-9]*,){0,1}[0-9]*(?:,[0-9]*){0,1}){$gapmin,$gapmax}";
			}else{
				$regex .= " (?:[0-9]*,){0,1}($item[0])(?:,[0-9]*){0,1}(?: (?:[0-9]*,){0,1}[0-9]*(?:,[0-9]*){0,1}){$gapmin,$gapmax}";
			}        
		}
	}
	if ($Motif[$#Motif] =~ m/\{+\S+\}/){
		$Motif[$#Motif] =~ s/\{//;
		$Motif[$#Motif] =~ s/\}//;

		my @item = split(/,/, $Motif[$#Motif]);    
		if(@item == 2){
			$regex .= " ($item[0],$item[1])(?: (?:[0-9]*,){0,1}[0-9]*(?:,[0-9]*){0,1}){$gapmin,$gapmax} ";
		}else{
			$regex .= " (?:[0-9]*,){0,1}($item[0])(?:,[0-9]*){0,1} ";
		}        
	}
	print "$regex\n";
	
	my $ligne = "";
	my $temp = 0;
	my $cpt_ligne = -1;
	my $found = 0;
	
	open (CORPUS, $corpus) or die("Erreur à l'ouverture du fichier '$corpus'");
	while (<CORPUS>){
		chomp $_;
		
		if($_ =~ m/seqId/){
			$cpt_ligne++;
			$ligne .= " ";
			if($ligne =~ m/$regex/p){
				$found++;
				
				
				my $lignetag = "";
				my $before_first = "";
				my $before_last = "";
				my $pos_first = $TabMots[$cpt_ligne-1]+1; 
				my $pos_last = $TabMots[$cpt_ligne-1]+1; 
				my $longueur = 0;
				
				$before_first = substr( $ligne, 1, $-[1]-1 );
				$before_last = $before_first; #cas où le motif est de taille 1:
				$lignetag .= $before_first;
				$lignetag .= "<b>".substr( $ligne, $-[1], $+[1]-$-[1] )."</b>";
				
				foreach my $exp (2..$#-) {
					$lignetag .= substr( $ligne, $+[$exp-1], $-[$exp]-$+[$exp-1] );
					$before_last = $lignetag;
					$lignetag .= "<b>".substr( $ligne, $-[$exp], $+[$exp]-$-[$exp] )."</b>";
				}
				$lignetag .= " ".${^POSTMATCH};					
				$lignetag =~ s/([0-9]+)/$Correspondance{$1}/g;

				$pos_first++ while $before_first =~ /\S+/g;
				$pos_last++ while $before_last =~ /\S+/g;
				
				$Res[0] =~ s/([0-9]+)/$Correspondance{$1}/g;
				$Res[0] =~ s/\s+//g;
				$longueur++ while $Res[0] =~ /\{/g;
				$HashMotifs{$pos_first}{$pos_last}[1] = $cpt_ligne;
				$HashMotifs{$pos_first}{$pos_last}[2] = $Res[0];
				$HashMotifs{$pos_first}{$pos_last}[3] = $longueur;
				$HashMotifs{$pos_first}{$pos_last}[4] = $lignetag;
				
				#print OUT "$cpt_ligne\t$pos_first\t$pos_last\t".$lignetag."\n";                
			}
			
			$ligne = "";
			$temp = 0;
		}else{
			my @LigneCourante = split (/\s/, $_);
			if($temp == $LigneCourante[0]){
				$ligne .= ",$LigneCourante[1]";
			}else{
				$temp = $LigneCourante[0];
				$ligne .= " $LigneCourante[1]";
			}
		}
	}
	
	#dernière ligne, on doit refaire une analyse.
	$cpt_ligne++;
	$ligne .= " ";
	if($ligne =~ m/$regex/p){
		my $lignetag = "";
		$lignetag .= substr( $ligne, 1, $-[1]-1 );
		$lignetag .= "<b>".substr( $ligne, $-[1], $+[1]-$-[1] )."</b>";
		foreach my $exp (2..$#-) {
			$lignetag .= substr( $ligne, $+[$exp-1], $-[$exp]-$+[$exp-1] );
			$lignetag .= "<b>".substr( $ligne, $-[$exp], $+[$exp]-$-[$exp] )."</b>";
		}
		$lignetag .= " ".${^POSTMATCH};
		$lignetag =~ s/([0-9]+)/$Correspondance{$1}/g;
		
		#print OUT "$cpt_ligne\t ".$lignetag."\n";            
	}		
	close(CORPUS) or die("Erreur à la fermeture");
    $cpt_motif ++;
	print $Res[0]." ".$found."\n";
}

print OUT "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
print OUT "<tei:TEI xmlns:tei=\"http://www.tei-c.org/ns/1.0\">\n";
print OUT "<teiHeader>\n";
print OUT "<fileDesc>\n";
print OUT "<titleStmt>\n";
print OUT "<title>$namecorpus - annotations</title>\n";
print OUT "<respStmt>\n";
print OUT "<resp>annotated by</resp>\n";
print OUT "<name>SDMC</name>\n";
print OUT "</respStmt>\n";
print OUT "</titleStmt>\n";
print OUT "<publicationStmt>\n";
print OUT "<publisher></publisher>\n";
print OUT "</publicationStmt>\n";
print OUT "<sourceDesc>\n";
print OUT "<bibl></bibl>\n";
print OUT "</sourceDesc>\n";
print OUT "</fileDesc>\n";
print OUT "</teiHeader>\n";
print OUT "<text></text>\n";
print OUT "<standOff>\n";
print OUT "<soHeader>\n";
print OUT "<titleStmt>\n";
print OUT "<title>URS annotations</title>\n";
print OUT "</titleStmt>\n";
print OUT "<respStmt>\n";
print OUT "<resp>\n";
print OUT "<name id=\"SDMC\">SDMC</name>\n";
print OUT "</resp>\n";
print OUT "</respStmt>\n";
print OUT "<revisionDesc>\n";
print OUT "<change who=\"#SDMC\" when=\"$date\">Created with SDMC</change></revisionDesc><encodingDesc>\n";
print OUT "<listPrefixDef>\n";
print OUT "<prefixDef ident=\"text\" matchPattern=\"(.+)\" replacementPattern=\"\"></prefixDef>\n";
print OUT "</listPrefixDef>\n";
print OUT "</encodingDesc>\n";
print OUT "</soHeader>\n";
print OUT "<annotations type=\"coreference\">\n";
print OUT "<annotationGrp type=\"Unit\" subtype=\"MOTIF\">\n";
#<span id="u-MOTIF-1" from="text:w_resu_6" to="text:w_resu_7" ana="#u-MOTIF-1-fs"></span>
$cpt_motif = 1;
foreach my $pos_first (sort {$a <=> $b} keys %HashMotifs) {
	foreach my $pos_last (sort {$a <=> $b} keys %{$HashMotifs{$pos_first}}) {
		print OUT "<span id=\"u-MOTIF-$cpt_motif\" from=\"text:w_resu_$pos_first\" to=\"text:w_resu_$pos_last\" ana=\"#u-MOTIF-$cpt_motif-fs\"></span>\n";
		if(not defined($HashChaine{$HashMotifs{$pos_first}{$pos_last}[2]})){
			$HashChaine{$HashMotifs{$pos_first}{$pos_last}[2]} = "#u-motif-$cpt_motif";
		}else{
			$HashChaine{$HashMotifs{$pos_first}{$pos_last}[2]} .= " #u-motif-$cpt_motif";
		}
		#print $HashMotifs{$pos_first}[2]." .= #u-motif-$cpt_motif\n";
		$cpt_motif++;
	}
}
print OUT "</annotationGrp>\n";
print OUT "<annotationGrp type=\"Schema\" subtype=\"CHAINE\">\n";
#<link id="s-CHAINE-1" target="#u-motif-10039 #u-motif-13491" ana="#s-CHAINE-1-fs"></link>
my $cpt_chaine = 1;
foreach my $motif (sort {lc $a cmp lc $b} keys %HashChaine) {
	print OUT "<link id=\"s-CHAINE-$cpt_chaine\" target=\"$HashChaine{$motif}\" ana=\"#s-CHAINE-$cpt_chaine-fs\"></link>\n";
	$cpt_chaine++;
}
print OUT "</annotationGrp>\n";
print OUT "<div type=\"unit-fs\">\n";
#<fs id="u-MOTIF-1-fs">
#<f name="longueur"><string>2</string></f>
#<f name="ref"><string>{NOM_}{NUM_}</string></f>
#<f name="numPhrase"><string>1</string></f>
#<f name="numPhrasePartie"><string>1</string></f>
#</fs>
$cpt_motif = 1;
foreach my $pos_first (sort {$a <=> $b} keys %HashMotifs) { #ATTENTION verifier que le u-motifs1 est bien le u-motif-1 etc (solution : créer new array dans la boucle Unit Motif
	foreach my $pos_last (sort {$a <=> $b} keys %{$HashMotifs{$pos_first}}) {
		print OUT "<fs id=\"u-MOTIF-$cpt_motif-fs\">\n";
		print OUT "<f name=\"longueur\"><string>$HashMotifs{$pos_first}{$pos_last}[3]</string></f>\n";
		print OUT "<f name=\"ref\"><string>$HashMotifs{$pos_first}{$pos_last}[2]</string></f>\n";
		print OUT "<f name=\"numPhrase\"><string>$HashMotifs{$pos_first}{$pos_last}[1]</string></f>\n";
		#print OUT "<f name=\"numPhrasePartie\"><string>1</string></f>";
		print OUT "</fs>\n";
		$cpt_motif++;
	}
}
print OUT "</div>\n";
print OUT "<div type=\"relation-fs\"></div>\n";
print OUT "<div type=\"schema-fs\">\n";
#<fs id="s-CHAINE-1-fs">
#<f name="GENRE"><string></string></f>
#<f name="NOMBRE"><string></string></f>
#<f name="TYPE REFERENT"><string></string></f>
#<f name="REF"><string>{NOM_}{NUM_}</string></f>
#<f name="NB MAILLONS"><string>96</string></f>
#</fs>
$cpt_chaine = 1;
foreach my $motif (sort {lc $a cmp lc $b} keys %HashChaine) {
	print OUT "<fs id=\"s-CHAINE-$cpt_chaine-fs\">\n";
	print OUT "<f name=\"GENRE\"><string></string></f>\n";
	print OUT "<f name=\"NOMBRE\"><string></string></f>\n";
	print OUT "<f name=\"TYPE REFERENT\"><string></string></f>\n";
	print OUT "<f name=\"REF\"><string>$motif</string></f>\n";
	my $nb_maillons = 0;
	$nb_maillons++ while $HashChaine{$motif} =~ /\S+/g;
	print OUT "<f name=\"NB MAILLONS\"><string>$nb_maillons</string></f>\n";
	print OUT "</fs>\n";
	$cpt_chaine++;
}
print OUT "</div>\n";
print OUT "</annotations>\n";
print OUT "</standOff>\n";
print OUT "</tei:TEI>";


close(OUT) or die("Erreur à la fermeture");
close(IN) or die("Erreur à la fermeture");


#print $pos_first." ";
#foreach my $intel (@{$HashMotifs{$pos_first}}) {
#	print $intel." ";
#}
#print "\n";
