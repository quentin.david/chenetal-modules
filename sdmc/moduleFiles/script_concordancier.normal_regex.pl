#!/usr/bin/perl -w

#use strict;
use warnings;
use utf8;

my ($uid,$file,$nl,$corpus,$gapmin,$gapmax,$decal) = @ARGV;

chdir("out/$uid/");
open (IN, $file) or die("Erreur à l'ouverture du fichier '$file'");
open (OUT, ">concordancier") or die("Erreur à l'ouverture du fichier 'concordancier'");
open (Correspondance, "correspondance") or die("Erreur à l'ouverture du fichier 'correspondance'");
binmode(STDOUT, ':utf8');
binmode(STDERR, ':utf8');

my %Correspondance;
while (<Correspondance>){
    chomp $_;
    my @Correspondance = split (/\t/, $_);
    $Correspondance{$Correspondance[1]} = $Correspondance[0];
}
close(Correspondance) or die("Erreur à la fermeture");

my $cpt_motif = 0;

while (<IN>){
    # Iterate lines until we reach our pattern
    if ($cpt_motif eq $nl){
        chomp $_;
        
        my @Res = split (/\:/, $_);

	# All the different tokens of the motif
        my @Motif = split (/\s/, $Res[0]);
		
	my $regex = "";
        
	# Iterate through all the tokens
	for (my $i = 0 ; $i <= $#Motif-1 ; $i++){        	
            if ($Motif[$i] =~ m/\{+\S+\}/){
		# Remove brackets
                $Motif[$i] =~ s/\{//;
                $Motif[$i] =~ s/\}//;
		
                my @item = split(/,/, $Motif[$i]);    
		# If there is two elements in the token (lem+pos?)		
                if(@item == 2){
		    $regex .= " ($item[0],$item[1])(?: (?:[0-9]*,){0,1}[0-9]*(?:,[0-9]*){0,1}){$gapmin,$gapmax}";
		    #$regex .= " ($item[0],$item[1]):([0-9]+)(?: (?:[0-9]*,){0,1}[0-9]*(?:,[0-9]*){0,1}){$gapmin,$gapmax}";
                }
		else{
			#$regex .= " (?:[0-9]*,){0,1}($item[0]):([0-9]+)(?:,[0-9]*){0,1}(?: (?:[0-9]*,){0,1}[0-9]*(?:,[0-9]*){0,1}){$gapmin,$gapmax}";
		    $regex .= " (?:[0-9]*,){0,1}($item[0])(?:,[0-9]*){0,1}(?: (?:[0-9]*,){0,1}[0-9]*(?:,[0-9]*){0,1}){$gapmin,$gapmax}";
                }        
            }
        }
	# Build regex for last pattern
	if ($Motif[$#Motif] =~ m/\{+\S+\}/){
		$Motif[$#Motif] =~ s/\{//;
		$Motif[$#Motif] =~ s/\}//;

		my @item = split(/,/, $Motif[$#Motif]);    
		if(@item == 2){
			$regex .= " ($item[0],$item[1])(?: (?:[0-9]*,){0,1}[0-9]*(?:,[0-9]*){0,1}){$gapmin,$gapmax} ";
		}else{
			$regex .= " (?:[0-9]*,){0,1}($item[0])(?:,[0-9]*){0,1} ";
		}        
	}
        
        my $ligne = "";
        my $temp = 0;
		my $cpt_ligne = -1;
       
	my $idx_line = "";
	print $regex;
        open (CORPUS, $corpus) or die("Erreur à l'ouverture du fichier '$corpus'");
        while (<CORPUS>){
            chomp $_;
	    # We reach a sentence marker 
            if($_ =~ m/seqId/){
		$cpt_ligne++;
		print $ligne."\n";
                $ligne .= " ";

		# We check if last sentence matches the pattern
                if($ligne =~ m/$regex/p){

			print "\n-------- START --------\n";
			print "\n\n------ Regex:\n".$regex;
			print "\n".$ligne;
			my $words = "Words: ";
			my $rest = "Rest: ";
			
			my $line = "";
			$line.= substr($ligne, 1, $-[1]-1);
			
			$words.= substr($ligne, $-[1], $+[1]-$-[1])." | ";

			foreach my $matched (2..$#-) {
				$rest .= substr($ligne, $+[$matched-1], $-[$matched]-$+[$matched-1])." | ";

				$words .= substr( $ligne, $-[$matched], $+[$matched]-$-[$matched] )." | ";
			}
			
			print "\n--------- Matched line\n\n";
			print $ligne;
			print "\n--------- Idx line\n\n".$idx_line;
			print "\n---------- Words \n\n";

			print $words;
			print "\n\n---------- Rest \n\n".$rest;

			$words =~ s/([0-9]+)/$Correspondance{$1}/g;
			print "\n\n\n---------- Words correspondance\n\n";
			print $words;
			

			# my $lignetag = "";

			# substr EXPR, OFFSET, LENGTH
			# Take a substring that goes from 1 to $-[1] minus one
			# $lignetag .= substr( $ligne, 1, $-[1]-1 );
			
			# $lignetag .= "\n";
			
			# Take a substring that goes from $-[1] to $+[1] minus $-[1]
			# $lignetag .= "<b>".substr( $ligne, $-[1], $+[1]-$-[1] )."</b>";
			
			# $lignetag .= "\n";

			# foreach my $exp (2..$#-) {
			#	$lignetag .= substr( $ligne, $+[$exp-1], $-[$exp]-$+[$exp-1] );
			#	$lignetag .= " : ";
			#	$lignetag .= "<b>".substr( $ligne, $-[$exp], $+[$exp]-$-[$exp] )."</b>";
			#}
			
			# ^POSTMATCH is everything after the match
			#$lignetag .= " ".${^POSTMATCH};					
			#$lignetag =~ s/([0-9]+)/$Correspondance{$1}/g;
			#

			#print OUT "$cpt_ligne\t ".$lignetag."\n";
			#

			print "\n--------  END  --------\n";
		}
		
		# The treatment of the sentence if finished
		# So we reinitialize those variables.		
                $ligne = "";
                $temp = 0;
		$idx_line = "";
            }else{
		# Only takes the first part of the line
		# but I think in the case of pos or lem+pos
		# it also takes the second line of the same token
		# (Dmt4 takes two lines if token has 2 informations)
                my @LigneCourante = split (/\s/, $_);
                if($temp == $LigneCourante[0]){
                    $ligne .= ",$LigneCourante[1]";
                }
		# Nous devrions construire une regex ici pour faire la correspondance entre les nombres matchés et les index
		else{
		    $idx_line .= $LigneCourante[1]." ";
                    $temp = $LigneCourante[0];
		    $ligne .= " $LigneCourante[1]";
		    #$ligne .= " $LigneCourante[1]:$LigneCourante[0]";
                }
            }
        }
        
		#dernière ligne, on doit refaire une analyse.
		$cpt_ligne++;
		$ligne .= " ";
		if($ligne =~ m/$regex/p){
			my $lignetag = "";
			$lignetag .= substr( $ligne, 1, $-[1]-1 );
			$lignetag .= "<b>".substr( $ligne, $-[1], $+[1]-$-[1] )."</b>";
			foreach my $exp (2..$#-) {
				$lignetag .= substr( $ligne, $+[$exp-1], $-[$exp]-$+[$exp-1] );
				$lignetag .= "<b>".substr( $ligne, $-[$exp], $+[$exp]-$-[$exp] )."</b>";
			}
			$lignetag .= " ".${^POSTMATCH};
			$lignetag =~ s/([0-9]+)/$Correspondance{$1}/g;
			
			print OUT "$cpt_ligne\t ".$lignetag."\n";            
		}		
        close(CORPUS) or die("Erreur à la fermeture");
        exit;
    }
    $cpt_motif ++;
}

close(OUT) or die("Erreur à la fermeture");
close(IN) or die("Erreur à la fermeture");
