<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="https://static.jstree.com/3.3.1/assets/dist/themes/default/style.min.css" />
</head>
<body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="https://static.jstree.com/3.3.1/assets/dist/jstree.min.js"></script>

<?php

function normalise_motif($motif)
{
 return trim(str_replace("\x01..\x1F"," ",mb_convert_case($motif,MB_CASE_LOWER,"UTF-8")));
}

function est_enfant($element_parent, $element_enfant)
{
 $element_parent = normalise_motif($element_parent);
 $element_enfant = normalise_motif($element_enfant);
 if($element_parent==$element_enfant) return false;
 $liste_motifs_parent = explode(" ",$element_parent);
 $liste_motifs_enfant = explode(" ",$element_enfant);
 $start = 0;
 foreach($liste_motifs_parent as $vp)
 {
  $start = array_search($vp,array_slice($liste_motifs_enfant,$start));
  if($start===false) return false;
 }
 return true;
}

function nouvel_element($motif)
{
 return array('motif'=>$motif, 'enfants'=>array(), 'nb_parents'=>0);
}

function hierarchise(array &$freres)
{
 foreach($freres as $p=>&$element_parent)
 {
  foreach($freres as $e=>&$element_enfant)
  {
   if(est_enfant($element_parent['motif'],$element_enfant['motif']))
   {
    $element_parent['enfants'][] = nouvel_element($element_enfant['motif']);
    $element_enfant['nb_parents']++;
   }
  }
 }
 // suppression des frères qui sont des enfants
 foreach($freres as $k=>$v)
 {
  if($v['nb_parents']>0) unset($freres[$k]);
 }
 // hierarchisation des enfants
 foreach($freres as &$element)
 {
  if(count($element['enfants'])>0) hierarchise($element['enfants']); // récursion sur les enfants !!!
 }
}

function cree_hierarchie(array $liste_motifs)
{
 $hierarchie = array();
 foreach($liste_motifs as $motif)
 {
  $hierarchie[] = nouvel_element($motif);
 }
 hierarchise($hierarchie);
 return $hierarchie;
}

//****** Test ******\\
$liste = array(
" DET:ART_ NOM_ chien ",
" DET:ART_ chien ",
" PRP_ NOM_ ",
" DET:ART_ NOM_ ADJ_ ",
" NAM_ ",
" PRP_ à ",
" DET:ART_ un NOM_ ",
" de PRP_ ",
" DET:ART_ NOM_ PRP_ ",
" NOM_ PRP_ ",
" PRP_ le DET:ART_ NOM_ ",
" PRO:PER_ ",
" NOM_ de PRP_ NOM_ ",
" DET:ART_ un ",
" NOM_ ",
" un ",
" NOM_ KON_ ",
" PRP_ le DET:ART_ ",
" et KON_ ",
" VER:infi_ ",
" être VER:pres_ ",
" PRP_ DET:ART_ NOM_ ",
" le DET:ART_ NOM_ ",
" NOM_ PRP_ NOM_ ",
" le DET:ART_ NOM_ PRP:det_ ",
" PRP:det_ NOM_ chien ",
" au ",
" du PRP:det_ NOM_ ",
" être VER:pres_ ADV_ ",
" PRP:det_ chien ",
" NOM_ chien VER:pres_ ",
" PRO:IND_ ",
" de PRP_ NOM_ ",
" DET:ART_ NOM_ PRP:det_ ",
" PRP:det_ NOM_ PRP_ ",
" que ",
" NOM_ chien PRP_ ",
" NOM_ le DET:ART_ NOM_ ",
" NOM_ ADJ_ PRP_ ",
" le DET:ART_ NOM_ PRP_ NOM_ ",
" VER:pres_ VER:infi_ ",
" NOM_ PRP_ le DET:ART_ NOM_ ",
" chien PRP_ ",
" VER:pres_ VER:pper_ ",
" PRP_ NOM_ KON_ ",
" KON_ DET:ART_ NOM_ ",
" PRP_ DET:ART_ ",
" KON_ le DET:ART_ ",
" ADV_ ADJ_ ",
" PRO:DEM_ ce ",
" le DET:ART_ ",
" PRP_ en NOM_ ",
" ce ",
" le ",
" NOM_ ADV_ ",
" en NOM_ ",
" ADV_ VER:pper_ ",
" DET:ART_ NOM_ de PRP_ NOM_ ",
" KON_ PRP_ ",
" ADV_ PRP_ ",
" avoir ",
" ADJ_ ",
" VER:pres_ PRP_ ",
" PRP_ par ",
" VER:pres_ DET:ART_ NOM_ ",
" par ",
" NOM_ PRP_ le DET:ART_ ",
" NOM_ VER:pper_ ",
" être ADV_ ",
" PRO:PER_ il ",
" PRP_ VER:infi_ ",
" il ",
" du PRP:det_ ",
" NOM_ le DET:ART_ ",
" KON_ DET:ART_ ",
" KON_ ou ",
" NUM_ NOM_ ",
" NOM_ de PRP_ ",
" PRO:REL_ ",
" DET:POS_ NOM_ ",
" PRP:det_ NOM_ ",
" NOM_ VER:pres_ ",
" PRP_ pour ",
" PRP_ dans ",
" ADJ_ KON_ ",
" dans ",
" le DET:ART_ NOM_ de PRP_ ",
" être VER:pper_ ",
" DET:POS_ ",
" NOM_ PRP_ DET:ART_ NOM_ ",
" VER:pres_ DET:ART_ ",
" PRP_ NOM_ PRP_ ",
" le DET:ART_ NOM_ VER:pres_ ",
" pour ",
" DET:ART_ NOM_ PRP_ NOM_ ",
" NOM_ DET:ART_ NOM_ ",
" DET:ART_ NOM_ ",
" le DET:ART_ NOM_ ADJ_ ",
" NOM_ PRP_ DET:ART_ ",
" PRP:det_ ",
" NUM_ ",
" DET:ART_ NOM_ VER:pres_ ",
" PRO:DEM_ ",
//" NOM_ NOM_ ",
" NOM_ chien ",
" VER:pres_ ",
" chien ",
" NOM_ DET:ART_ ",
" DET:ART_ NOM_ de PRP_ ",
" NOM_ du PRP:det_ NOM_ ",
" NOM_ et KON_ ",
" le DET:ART_ NOM_ chien ",
" NOM_ être VER:pres_ ",
" le DET:ART_ chien ",
" VER:pper_ ",
" DET:ART_ ",
" le DET:ART_ NOM_ PRP_ ",
" ADJ_ PRP_ ",
" PRP_ en ",
" NOM_ ADJ_ ",
" être ",
" NOM_ du PRP:det_ ",
" VER:pres_ ADV_ ",
" ADJ_ NOM_ ",
" NOM_ PRP:det_ NOM_ ",
" PRO:PER_ VER:pres_ ",
" PRP_ ",
" ADV_ ",
" VER:pper_ PRP_ ",
" NOM_ être ",
" NOM_ PRP:det_ ",
" en ",
" KON_ ",
);


$toto =cree_hierarchie($liste);
function html_motifs(&$h)
{
 echo "<ul>";
 //$t = array();
 foreach($h as $v)
 {
  echo "<li>".$v['motif']."<br/>";
  //$t[$v['motif']] = get_motifs($v['enfants']);
  html_motifs($v['enfants']);
  echo "</li>";
 }
 //return $t;
 echo "</ul>";
}
echo "<div id=\"arbre\">";
html_motifs($toto);
echo "</div>";
echo "<pre>";
var_dump($liste);
echo "\n";

//var_dump($toto);
//echo "\n";
echo "</pre>";
?>

<script>
$('#arbre').jstree();
</script>

</body>
</html>