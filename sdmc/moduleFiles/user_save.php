<?php

require("web/config/config.php");

$page = (isset($_GET["p"])) ? $_GET["p"] : "form";
function deleteDirectory($dir) {
    if ($dir == "out/") return true;
    if (!file_exists($dir)) return true; 
    if (!is_dir($dir) || is_link($dir)) return unlink($dir); 
    foreach (scandir($dir) as $item) { 
        if ($item == '.' || $item == '..') continue; 
        if (!deleteDirectory($dir . "/" . $item)) { 
            chmod($dir . "/" . $item, 0777); 
            if (!deleteDirectory($dir . "/" . $item)) return false; 
        }; 
    } 
    return rmdir($dir); 
} 
// on vérifie que l'utilisateur est bien connecté
if (!isset($_SESSION["email"])) {
    header("Location: index.php?p=forbidden");
}
// on récupère les infos sur l'utilisateur courant
$currentUser = $userDB->getUser($_SESSION["email"]);

// on récupère les corpus de l'utilisateur courant
$corpus = $corpusDB->getAllCorpus($currentUser->getId());
$currentUser->setCorpus($corpus);

try {
	switch ($page) {	
	    
	    case "profile" :
	    	$title = "Gestion du profil";
	    	$content = Template::render("profileForm", array("user" => $currentUser));
	    	unset($_SESSION["success"]);
	    	unset($_SESSION["error"]);
	    break;
	    
	    case "updateProfile" :
	    	$userDB->saveProfile($_POST, $currentUser);
	    	header("Location: user.php?p=profile");
	    break;
	    
	    case "manager" :
	    	$title = "Gestion des traitements";
	    	$processes = $processDB->getProcesses($currentUser);
	    	$content = Template::render("printProcesses", array("processes" => $processes));
	    break;	
	    
		case "cancel" :
			$process = $processDB->getProcessById($_GET["id"]);
	    	exec('pkill -KILL -P ' . $process->getPid());
	    	$processDB->removeProcess($process->getPid());
			$name = $process->getName(); 
			if(isset($name)){
				if($name != ""){
					$path = "out/" . $name;
					if(is_dir($path)) {
						//unlink($path);
						deleteDirectory($path);
					}
				}
			}
	    	header("Location: user.php?p=manager");
	    break;	
	    
	    case "form" :				    	
	    	$category = (isset($_GET["category"])) ? $_GET["category"] : "linguists";
			$form = (isset($_GET["form"])) ? $_GET["form"] : "sequential";
            if($form != "concordancier"){
                $content = Template::render("form/" . $form . "." . $category, array("corpus" => $currentUser->getCorpus()));
                $title = Template::getLabel($form);
            }else{
                $processes = $processDB->getProcesses($currentUser);
                $content = Template::render("form/" . $form . "." . $category, array("processes" => $processes));
                $title = "Concordancier";
            }
			
	    break;

        case "choixMotif" :
            $category = (isset($_GET["category"])) ? $_GET["category"] : "linguists";
			$form = (isset($_GET["form"])) ? $_GET["form"] : "sequential";
            $process = $processDB->getProcessById($_GET["id"]);
            $corpus = $corpusDB->getCorpusByName($process->getCorpusname());
            $content = Template::render("form/concordancier.motif." . $category, array("process" => $process,"corpus" => $corpus));
            $title = "Concordancier";
        break;
        
        case "calculConcordancier" :
            $category = (isset($_GET["category"])) ? $_GET["category"] : "linguists";
			$form = (isset($_GET["form"])) ? $_GET["form"] : "sequential";
            $nl = (isset($_GET["nl"])) ? $_GET["nl"] : "0";
            $process = $processDB->getProcessById($_GET["id"]);
            $corpus = $corpusDB->getCorpusByName($process->getCorpusname());
            //$content = '<div id="response">Calcul en cours, merci de patienter.</div>';  
            $content = Template::render("form/concordancier.affichage." . $category, array("process" => $process,"corpus" => $corpus,"nl" => $nl));
            $title = "Concordancier";
        break;
    
    
        case "test_htsa":
            $process = $processDB->getProcessById($_GET["id"]);
            $corpus = $corpusDB->getCorpusByName($process->getCorpusname());
            $test = $currentUser->getCorpus();
            $content = Template::render("form/stage", array("process" => $process,"corpus" => $corpus));
            $title ="La liste des motifs présent dans le texte ";
	break;
    
    case "scriptbis" :
            $category = (isset($_GET["category"])) ? $_GET["category"] : "linguists";
			$form = (isset($_GET["form"])) ? $_GET["form"] : "sequential";
            $nl = (isset($_GET["nl"])) ? $_GET["nl"] : "0";
            $process = $processDB->getProcessById($_GET["id"]);
            $corpus = $corpusDB->getCorpusByName($process->getCorpusname());
            //$content = '<div id="response">Calcul en cours, merci de patienter.</div>';  
            $content = Template::render("form/scriptbis", array("process" => $process,"corpus" => $corpus,"nl" => $nl));
            $title = "Concordancier";
        break;
     
    
	    case "run" :
	    	try {
	    		$category = (isset($_GET["category"])) ? $_GET["category"] : "linguists";
				$form = (isset($_GET["form"])) ? $_GET["form"] : "sequential";
				$newCorpus = ($_POST["corpusAlreadyUsed"] == "yes") ? true : false;
                $binaire = (isset($_POST["tool"])) ? $_POST["tool"] : "null"; 
                
				// si on souhaite utiliser un corpus déjà utilisé
				if($newCorpus) {
					$corpus = $corpusDB->getOneCorpus($_POST["oldCorpus"]);					
				} 
				// sinon, on souhaite uploader un nouveau corpus
				else {
					$lang = (isset($_POST["lang"])) ? $_POST["lang"] : 0;
					$format = (isset($_POST["format"])) ? $_POST["format"] : 0;
		 	    	$corpus = Corpus::createFromFiles($_FILES, $currentUser, $lang, $format, $category);
		 	    	// on ajoute le corpus en bdd
					$corpusDB->addCorpus($corpus);
					$currentUser->addCorpus($corpus);
				}	    
				$process = new Process(array(
					"user" => $currentUser,
					"category" => $category,
					"form" => $form,
					"corpus" => $corpus,
                    "corpusname" => $corpus->getName(),
                    "binaire" => $binaire,
                    "gapmin" => $_POST["gapmin"],
                    "gapmax" => $_POST["gapmax"],
                    "nbitemsetmin" => $_POST["nbitemsetmin"],
                    "nbitemsetmax" => $_POST["nbitemsetmax"],
                    "minsup" => $_POST["minsup"]    
				));
				// on appelle la méthode de la classe qui correspond au processus qu'on veut lancer				
				$message = $process->launch();
                
				
				
	    	} catch(Exception $e) {
	    		$message = $e->getMessage(); 
	    	}	
	    	// code html de la popup
			$content = '<div id="response">'. $message .'</div>';    		    	
			// on renvoie le select des corpus
			$content .= Template::render("form/selectCorpus", array("corpus" => $currentUser->getCorpus()));
		break;
		
		case "rmProcess" :
			// on récupère le processus
			$process = $processDB->getProcessById($_GET["id"]);
			// on supprime le processus en bd
			$processDB->removeProcessById($_GET["id"]);
			// on supprime le dossier de résultat
			$name = $process->getName(); 
			if(isset($name)){
				if($name != ""){
					$path = "out/" . $name;
					if(is_dir($path)) {
						//unlink($path);
						deleteDirectory($path);
					}
				}
			}
			header("Location: user.php?p=manager");
		break;
		
		case "corpus" :
			$corpus = $currentUser->getCorpus();
			$content = Template::render("printCorpus", array("corpus" => $corpus));
			$title = "Gérer vos corpus";
		break;
            
		
		case "rmCorpus" :
			$corpus = $corpusDB->getOneCorpus($_GET["id"]);
			if(is_file("corpus/" . $corpus->getPath())) {
				unlink("corpus/" . $corpus->getPath());
			}
			$corpusDB->deleteCorpus($corpus->getId());
			header("Location: user.php?p=corpus");
		break;
				
	    default :
		$title = "Page non trouvée";
		$content = "Erreur 404";
		break;
	} // end switch
} // end try
catch(Exception $e) {
	$title = "Erreur";
	$content = $e->getMessage();
}
if(!defined("AJAX")) {
	include("web/ui/pages/page.php");
} else {
	if(isset($content)) {
		echo $content;
	}	
}
?>
