#!/usr/bin/perl -w

use strict;
use warnings;
use utf8;

# ./rewrite.pl Dmt4_test.txt 

open (In, $ARGV[0]) or die;
binmode(In, ':utf8');
open (Out, '> '.$ARGV[0].'ok') or die;
binmode(Out, ':utf8');
my $prec = '';
my $itemset = 0;
my $first = 1;
my @list;
my @line;
foreach (<In>){
	chomp $_;
	if ($_ =~ /seqId/){
		if ($itemset != 0){
			foreach my $item (sort { $a <=> $b } @list){ #pour trier
				if ($itemset.' '.$item ne $prec){
					print Out "\n".$itemset.' '.$item;
					$prec = $itemset.' '.$item;
				}
			}
			undef @list;
		}
		if ($first){
			$first = 0;
		}else{
			print Out "\n";
		}
		print Out $_;
		undef @list;
	}else{
		@line = split(/\s/, $_);
		if ($itemset != $line[0]){
			if ($itemset != 0){
				foreach my $item (sort { $a <=> $b } @list){
					if ($itemset.' '.$item ne $prec){
						print Out "\n".$itemset.' '.$item;
						$prec = $itemset.' '.$item;
					}
				}
				undef @list;
			}
			$itemset = $line[0];
		}
		push (@list, $line[1]);
	}
}
if ($itemset != 0){
	foreach my $item (sort { $a <=> $b } @list){
		if ($itemset.' '.$item ne $prec){
			print Out "\n".$itemset.' '.$item;
			$prec = $itemset.' '.$item;
		}
	}
	undef @list;
}

# foreach (<In>){
# 	if ($_ ne $prec){
# 		print Out $_;
# 	}
# 	$prec = $_;
# }