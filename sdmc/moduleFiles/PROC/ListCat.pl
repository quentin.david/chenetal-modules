#!/usr/bin/perl -w

use strict;
use warnings;
use utf8;

open (In, $ARGV[0]) or die; #fichier des correspondances
open (Cat, $ARGV[1]) or die "$ARGV[1]"; #fichier des choix utilisateur
my $DIR_TOOL = $ARGV[2];
my $LANG = $ARGV[3];
binmode(Cat, ':utf8');
binmode(In, ':utf8');
# binmode(STDOUT, ':utf8');

&main();

sub WriteResult {
	my $Hash;
	my $Key;
	my $line;
	my $Choice;
	my @Tags;
	my $Exist = 0;
	($Hash, $Key, $line, $Choice, @Tags) = @_;
	if (exists($Choice->{1+$Key}))
	{
		foreach (@Tags)
		{
			$Exist = ($line->[0] =~ /$_/) || $Exist;
		}
		if ($Exist)
		{
			if (exists($Hash->{$Key}))
			{
				$Hash->{$Key} .= ','.$line->[1];
			}
			else
			{
				$Hash->{$Key} = $line->[1];
			}
		}
	}
	return $Exist;
}

sub main {

	my %Result;
	my $Result = "";

	my %Choice;
	foreach (<Cat>)
	{
		chomp $_;
		$Choice{$_} = 1;
	}

	close Cat;

	if (!exists($Choice{0})) # si l'utilisateur a choisi une contrainte d'appartenance comme nom, verbe, etc.
	{
		if ($LANG == 0)
		{
			foreach (<In>)
			{
				chomp $_;
				my @line = split(/\s/, $_);
				if ($line[0] =~ /_/)
				{
					if (!(&WriteResult(\%Result, 0, \@line, \%Choice, "NN", "NP")))
					{
						if (!(&WriteResult(\%Result, 1, \@line, \%Choice, "VB")))
						{
							if (!(&WriteResult(\%Result, 2, \@line, \%Choice, "JJ")))
							{
								&WriteResult(\%Result, 3, \@line, \%Choice, "RB");
							}
						}
					}
				}
			}
		}
		else
		{
			if ($LANG == 1)
			{
				foreach (<In>)
				{
					chomp $_;
					my @line = split(/\s/, $_);
					if ($line[0] =~ /_/)
					{
						if (!(&WriteResult(\%Result, 0, \@line, \%Choice, "NOM", "NAM")))
						{
							if (!(&WriteResult(\%Result, 1, \@line, \%Choice, "VER")))
							{
								if (!(&WriteResult(\%Result, 2, \@line, \%Choice, "ADJ")))
								{
									&WriteResult(\%Result, 3, \@line, \%Choice, "ADV");
								}
							}
						}
					}
				}
			}
		}
		foreach (sort keys(%Result))
		{
			$Result .= $Result{$_}.',';
		}
		chop $Result;
		
		open (Load, "../$DIR_TOOL/Load.ini") or die; #fichier ini de l'extracteur de motifs
		open (LoadNew, '> '.$ARGV[0].'temp') or die "$ARGV[0]".'temp'; #fichier des choix utilisateur
		binmode(Load, ':utf8');
		binmode(LoadNew, ':utf8');
		foreach (<Load>)
		{
			print LoadNew $_;
		}
		close Load;
		print LoadNew "\nOR=$Result";
		close LoadNew;
		system("cp -f ".$ARGV[0]."temp ../$DIR_TOOL/Load.ini");
	# 	system("echo -e \"\nOR=$Result\" > Load.ini`");

	}
	close In;

}
