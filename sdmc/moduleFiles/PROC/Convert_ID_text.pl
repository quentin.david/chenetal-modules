#!/usr/bin/perl -w

use strict;
use warnings;
use utf8;

open (Result, $ARGV[0]) or die;
open (Correspondance, $ARGV[1]) or die "";
# open (Out, '> '.$ARGV[0].'.text') or die "";
binmode(STDOUT, ':utf8');
binmode(Result, ':utf8');
binmode(Correspondance, ':utf8');
# binmode(Out, ':utf8');

my %Correspondance;
foreach (<Correspondance>){
	chomp $_;
	my @Correspondance = split (/\t/, $_);
	$Correspondance{$Correspondance[1]} = $Correspondance[0];
}

foreach (<Result>){
	chomp $_;
	my @Result = split (/:/, $_);
	$Result[1] =~ s/\s+//;
	my @Itemset = split (/\s/, $Result[0]);
	foreach my $Itemset (@Itemset){
		$Itemset =~ s/\{|\}//g;
		my @item = split(/,/, $Itemset);
		print '{';
		print $Correspondance{$item[0]};
		for (my $i = 1 ; $i <= $#item ; $i++){
			print ' '.$Correspondance{$item[$i]};
		}
# 		foreach my $item (@item){
# 			print Out $Correspondance{$item}.' ';
# 		}
		print '}';
	}
	print "\t".'sup='.$Result[1]."\n";
}

# close Out;