#!/usr/bin/perl -w

use strict;
use warnings;
use utf8;

open (In, $ARGV[0]) or die "Erreur ouverture de $ARGV[0]";
binmode(In, ':utf8');
open (Dmt4, "> Dmt4_$ARGV[0]") or die "Erreur ouverture de Dmt4_$ARGV[0]";
binmode(Dmt4, ':utf8');
open (Dico, "> Correspondance_$ARGV[0]") or die "Erreur ouverture de Correspondance_$ARGV[0].txt";
binmode(Dico, ':utf8');
my %Dico; # hash faisant la correspondance mot <-> id
my $Dico_pos = 0; # id du dernier mot nouveau rencontré
my $seqId = 0; # id de la séquence courante
my $Itemset = 0; # id de l'itemset courant
my $begin = 1; # vaut 1 si nous débutons une nouvelle séquence, 0 sinon
my $first_entry = 1; # indique si nous rentrons pour la première fois dans le code
my $choix = $ARGV[1];
#  line[0] = forme, line[1] = catégorie, line[2] = lemme, line[3] = lemme + cat

foreach (<In>)
{
	chomp $_;
	my @line = split (/\t+/, $_);
	if ($line[1] eq 'SENT')
	{ # si nous rencontrons un marqueur de fin de phrase, nous indiquons : nouvelle séquence
# 		print Out ".\n";
		$begin = 1;
		$Itemset = 0;
	}
	else
	{
		if ($begin)
		{ # si nouvelle séquence la ligne précédente
			$begin = 0;
			if ($first_entry)
			{ # si première ligne, pas de saut de ligne à faire
				$first_entry = 0;
			}
			else
			{
				print Dmt4 "\n";
			}
			print Dmt4 'seqId '.++$seqId;
		}
		
		if ($line[2] ne '<unknown>')
		{
			# We actually want to keep the punctuation.
			#if ($line[0] !~ /^"$|^,$|^;$|^:$|^\[$|^\]$|^\)$|^\($|^''$|^'$|^#$|^\+$|^=$/g)
			#{ # on ne conserve pas la ponctuation
			$line[1] .= '_'; # ajout de "_cat" aux catégorie grammaticales
			$line[2] = lc($line[2]); # passage des lemmes en minuscules
			++$Itemset; # incrémentation de l'id itemset
			if ($choix == 3) # lemme + cat
			{
				foreach (my $int = 2 ; $int >= 1 ; $int--)
				{
					if ($line[$int] ne '')
					{
						if (!exists $Dico{$line[$int]})
						{ # si nous avons un nouveau mot, on lui affecte un id
							$Dico{$line[$int]} = ++$Dico_pos;
						}
# 								print Dmt4 "\n".$Itemset.' '.$Dico{$line[$int]};
						print Dmt4 "\n".$Itemset.' '.$Dico{$line[$int]};
					}
				}
			}
			else
			{
				if ($line[$choix] ne '')
				{
					if (!exists $Dico{$line[$choix]})
					{ # si nous avons un nouveau mot, on lui affecte un id
						$Dico{$line[$choix]} = ++$Dico_pos;
					}
					print Dmt4 "\n".$Itemset.' '.$Dico{$line[$choix]};
				}
			}
		}
		#}
	}
}

foreach (sort {$Dico{$a} <=> $Dico{$b}} keys %Dico){ # trie des mots par id et écriture de la table de correspondance, format : id <tabulation> mot
	print Dico $_."\t".$Dico{$_}."\n";
}
close Dmt4;
close In;
close Dico;
