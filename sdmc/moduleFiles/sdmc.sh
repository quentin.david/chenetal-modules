#!/bin/bash
#Script pour utiliser SDMC en ligne de commande
#Pour toute question : pierre.holat@lipn.univ-paris13.fr
#
#Ce script utilise Treetager
#http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/


if [[ $1 == "help" ]]
then
	printf "\nUsage :\n"
	printf "./sdmc.sh [CORPUS] [SORTIE] [LANG] [TOOL] [CHOIX] [FORMAT] [MINGAP] [MAXGAP] [MINSIZE] [MAXSIZE] [MINFREQ]\n\n"
	printf "[CORPUS] chemin ABSOLU du corpus d'entrée !\n"
	printf "[SORTIE] nom du DOSSIER de sortie, il sera crée dans SDMC_terminal/out/\n"
	printf "[LANG] langue du corpus d'entrée :\n\t'en' : Anglais\n\t'fr' : Francais\n"
	printf "[TOOL] exécutable de fouille de motifs :\n\t'PrefixConstraint' : motifs fréquents\n\t'BIDESpanTree' : motifs fréquents fermés\n\t'MaxSpanTree' : motifs fréquents maximaux\n"
	printf "[CHOIX] type de vocabulaire à extraire :\n\t'mot' : Forme du mot\n\t'pos' : Postag du mot\n\t'lem' : Lemme du mot\n\t'lempos' : Postag et lemme du mot\n"
	printf "[FORMAT] format du corpus d'entrée :\n\t'txt' : texte brut\n\t'tt' : texte déjà treetagé (sortie directe du treetagger)\n"
	printf "[MINGAP] nombre minimal de mots (relatif au texte d'entrée) entre deux itemsets d'un motif\n"
	printf "[MAXGAP] nombre maximal de mots (relatif au texte d'entrée) entre deux itemsets d'un motif\n"
	printf "[MINSIZE] nombre minimal d'itemsets dans un motif\n"
	printf "[MAXSIZE] nombre maximal d'itemsets dans un motif\n"
	printf "[MINFREQ] nombre minimal d’occurrences d'un motif dans le texte (Valeur entière !)\n\n"

	printf "Exemple :\n"
	printf "./sdmc.sh /home/holat/Data/corpus_texte_fr.txt Exp_0_0_1_10_50 fr BIDESpanTree lempos txt 0 0 1 10 50\n\n"

	printf "Un problème avec le script ? \n"
	printf "pierre.holat@lipn.univ-paris13.fr (Pensez à joindre le fichier exec.log)\n\n"
	exit;
fi

CORPUS=$1 # nom du corpus donné par l'utilisateur
CORPUS_TAG=$2 # nom dossier resultat
LANG=$3 # 0 = en, 1 = fr
TOOL=$4 # nom de l'exécutable de l'extracteur de motif + nom du répertoire
CHOIX=$5 # 0 Forme du mot seul 2 Lemme du mot seul 1 Catégorie syntaxique du mot seul 3 Catégorie syntaxique et lemme du mot
FORMAT=$6 #0 Texte brut 1 Tree-Tagger
GAPMIN=$7
GAPMAX=$8
NBITEMSETMIN=$9
NBITEMSETMAX=${10}
MINSUP=${11}


# Modifications par Quentin pour chenetal:
# Ajout du relative path pour pouvoir appeler le script d'un autre répertoire que celui de ce fichier.

SCRIPTPATH=$(realpath $0)
SCRIPTFOLDER=`dirname $SCRIPTPATH`

DIR="$SCRIPTFOLDER/PROC" # répertoire de travail (contenant tous les sous scripts)
PATTERNS="$SCRIPTFOLDER/out$2" # nom du fichier de sortie
OUTPUTDIR="$SCRIPTFOLDER/out/$2/"
TMPDIR="$SCRIPTFOLDER/out"
BIN="$SCRIPTFOLDER/binaire"

if [ ! -f $CORPUS ]; then
	printf "Erreur, corpus introuvable.\n[CORPUS] chemin ABSOLU du corpus d'entrée !\nFin du programme.\n"
	exit
fi

#creation dossier resultat
if [ ! -d "$TMPDIR/$CORPUS_TAG" ]; then
	mkdir $TMPDIR/$CORPUS_TAG
	printf "Création dossier $TMPDIR/$CORPUS_TAG.\n" >> $TMPDIR/$CORPUS_TAG/exec.log
else
	printf "Erreur, le répertoire de sortie existe déjà.\nFin du programme pour éviter la suppression des données déjà existantes.\n"
	exit
fi


if [ $TOOL == "PrefixConstraint" ] || [ $TOOL == "BIDESpanTree" ] || [ $TOOL == "MaxSpanTree" ]
then
	cp $BIN/$TOOL $TMPDIR/$CORPUS_TAG/$TOOL
	printf "Copie binaire $TMPDIR/$CORPUS_TAG/$TOOL.\n" >> $TMPDIR/$CORPUS_TAG/exec.log
else
	printf "Erreur, TOOL inconnu.\n[TOOL] exécutable de fouille de motifs :\n\t'PrefixConstraint' : motifs fréquents\n\t'BIDESpanTree' : motifs fréquents fermés\n\t'MaxSpanTree' : motifs fréquents maximaux\nFin du programme.\n"
	rm -rf $TMPDIR/$CORPUS_TAG
	exit
fi

#Treetagger
cd $DIR
if [[ $FORMAT == "tt" ]] #Treetag
then 
	cp $CORPUS ./$CORPUS_TAG
	printf "Format TreeTagger.\n" >> $TMPDIR/$CORPUS_TAG/exec.log
elif [[ $FORMAT == "txt" ]]
then
	printf "Format Texte.\n" >> $TMPDIR/$CORPUS_TAG/exec.log
	if [[ $LANG == "en" ]] # permet de géner le français ou l'anglais pour le lancement du treetagger
	then 
		printf "Treetager en :\n" >> $TMPDIR/$CORPUS_TAG/exec.log
		Tag/cmd/tree-tagger-english-utf8 $CORPUS > $CORPUS_TAG 2>> $TMPDIR/$CORPUS_TAG/exec.log

	elif [[ $LANG == "fr" ]]
	then 
		printf "Treetager fr :\n" >> $TMPDIR/$CORPUS_TAG/exec.log
		Tag/cmd/tree-tagger-french-utf8 $CORPUS > $CORPUS_TAG 2>> $TMPDIR/$CORPUS_TAG/exec.log
	else
		printf "Erreur, LANG inconnu.\n[LANG] langue du corpus d'entrée :\n\t'en' : Anglais\n\t'fr' : Francais\nFin du programme.\n"
		rm -rf $TMPDIR/$CORPUS_TAG
		exit
	fi
else
	printf "Erreur, FORMAT inconnu.\n[FORMAT] format du corpus d'entrée :\n\t'txt' : texte brut\n\t'tt' : texte déjà treetagé (sortie directe du treetagger)\nFin du programme.\n"
	rm -rf $TMPDIR/$CORPUS_TAG
	exit
fi

cp $CORPUS_TAG $TMPDIR/$CORPUS_TAG/Treetag_data

printf "RewriteTag :\n" >> $TMPDIR/$CORPUS_TAG/exec.log
if [[ $CHOIX == "mot" ]]
	then ./RewriteTag.pl $CORPUS_TAG 0 2>> $TMPDIR/$CORPUS_TAG/exec.log # écrit le fichier d'entré de l'extracteur de motif, ainsi que le fichier des correspondances (mot <-> id). PROPRE AU TREETAGGER.
elif [[ $CHOIX == "pos" ]]
	then ./RewriteTag.pl $CORPUS_TAG 1 2>> $TMPDIR/$CORPUS_TAG/exec.log
elif [[ $CHOIX == "lem" ]]
	then ./RewriteTag.pl $CORPUS_TAG 2 2>> $TMPDIR/$CORPUS_TAG/exec.log
elif [[ $CHOIX == "lempos" ]]
	then ./RewriteTag.pl $CORPUS_TAG 3 2>>$TMPDIR/$CORPUS_TAG/exec.log
else
	printf "Erreur, CHOIX inconnu.\n[CHOIX] type de vocabulaire à extraire :\n\t'mot' : Forme du mot\n\t'pos' : Postag du mot\n\t'lem' : Lemme du mot\n\t'lempos' : Postag et lemme du mot\nFin du programme.\n"
	rm -rf $TMPDIR/$CORPUS_TAG
	exit
fi


printf "Rewrite :\n" >> $TMPDIR/$CORPUS_TAG/exec.log
./rewrite.pl Dmt4_$CORPUS_TAG 2>>$TMPDIR/$CORPUS_TAG/exec.log # gérere un fichier d'entré 'propre' (sans doublon et avec itensets triés)

cp Dmt4_${CORPUS_TAG}ok $TMPDIR/$CORPUS_TAG/Dmt4_data

cd $TMPDIR/$CORPUS_TAG
echo -e "GAPMIN=$GAPMIN" > Load.ini
echo -e "GAPMAX=$GAPMAX" >> Load.ini
echo -e "NB_ITEMSET_MIN=$NBITEMSETMIN" >> Load.ini
echo -e "NB_ITEMSET_MAX=$NBITEMSETMAX" >> Load.ini
echo -e "MINSUP=$MINSUP" >> Load.ini
echo -e "CORPUS=Dmt4_data" >> Load.ini


printf "Extraction :\n" >> exec.log
./$TOOL > resultat_0 2>>exec.log # exécution de l'extracteur

cd $DIR

printf "Convert_ID_text :\n" >> $TMPDIR/$CORPUS_TAG/exec.log
./Convert_ID_text.pl $TMPDIR/$CORPUS_TAG/resultat_0 Correspondance_$CORPUS_TAG > $PATTERNS.correspondance 2>>$TMPDIR/$CORPUS_TAG/exec.log # écriture du fichier contrainte les motifs avec les mots correspondants


# écriture d'un message si le fichier de sortie est vide
if [ ! -s $PATTERNS.correspondance ]; then
    printf "Le fichier est vide." > $PATTERNS.correspondance
	printf "résultat vide\n" >> $TMPDIR/$CORPUS_TAG/exec.log
else
	printf "résultat ok\n" >> $TMPDIR/$CORPUS_TAG/exec.log
fi

mv $PATTERNS.correspondance $TMPDIR/$CORPUS_TAG/resultat_correspondance # sauvegarde du fichier de sortie

rm -f *$CORPUS_TAG* # suppression des fichiers temporaires

rm $TMPDIR/$CORPUS_TAG/$TOOL
rm $TMPDIR/$CORPUS_TAG/Load.ini
rm $TMPDIR/$CORPUS_TAG/Dmt4_data
rm $TMPDIR/$CORPUS_TAG/resultat_0
if [ -f $TMPDIR/$CORPUS_TAG/TIME ]; then
	rm $TMPDIR/$CORPUS_TAG/TIME
fi

