<?php

require_once("web/config/config.php");

$page = (isset($_GET["p"])) ? $_GET["p"] : "users";

// on vérifie que l'utilisateur est bien connecté
if (!isset($_SESSION["email"])) {
    header("Location: index.php?p=forbidden");
}
// on récupère les infos sur l'utilisateur courant
$currentUser = $userDB->getUser($_SESSION["email"]);

// on vérifie que l'utilisateur est admin
if(!$currentUser->isAdmin()) {
	header("Location: index.php?p=forbidden");
}

try {
	switch ($page) {	   	    
	    case "users" : 	
    		$title = "Utilisateurs";
    		$users = $userDB->getUsers();
    		$content = Template::render("listUsers", array("users" => $users));
	    break;
	    	
	    case "addUser" :
    		$title = "Ajouter un utilisateur";
    		$content = Template::render("addUser", array("action" => "admin.php?p=addUser"));
    		if(!empty($_POST)) {
    			$userDB->addUser($_POST);
    			header("Location: admin.php?p=users");
    		}
	    break;
	    
	    case "activateAccount" :
	    	$user = $userDB->getUserById($_GET["id"]);
	    	$password = uniqid();
	    	$user->setPassword($password);
	    	$userDB->activateAccount($user);
	    	$mailer->activateAccount($user);
	    	header("Location: admin.php?p=users");
	    break;
	    	    
	    case "rmUser" :
	    	if(!isset($_GET["id"])) throw new Exception("Pas d'id");
	    	$user = $userDB->getUserById($_GET["id"]);
    		$title = "Supprimer un utilisateur";
    		$content = Template::render("rmUser", array("user" => $user));
    		if(isset($_GET["conf"])) {
    			$userDB->deleteUser($user->getId());
    			$mailer->accountDeleted($user->getEmail());
    			header("Location: admin.php?p=users");
    		}
	    break;
	    
	    case "logs" : 
			$type = (isset($_GET["logs"])) ? $_GET["logs"] : "users"; 
			$title = ($type == "users") ? "Utilisations" : "Erreurs";
			if($type == "users") {
				$logs = LogUser::getLogs();
			} else {
				$logs = LogError::getLogs();
			}
			$content = Template::render("logs", array("logs" => $logs, "type" => $type));
		break;
		
		case "emptyLog" :
			file_put_contents("logs/" . $_GET["logs"] . ".log", "");			
			header("Location: admin.php?p=logs&logs=" . $_GET["logs"]);
		break;
			    
	    default :
		$title = "Page non trouvée";
		$content = "Erreur 404";
		break;
	} // end switch
} // end try
catch(Exception $e) {
	$title = "Erreur";
	$content = $e->getMessage();
}

include("web/ui/pages/page.php");
?>
