#!/bin/bash
CORPUS=$1 # nom du corpus donné par l'utilisateur
CORPUS_TAG=$2 # nom généré aléatoirement
LANG=$3 # 0 = en, 1 = fr
TOOL=$4 # nom de l'exécutable de l'extracteur de motif + nom du répertoire
CHOIX=$5 # 0 Forme du mot seul 2 Lemme du mot seul 1 Catégorie syntaxique du mot seul 3 Catégorie syntaxique et lemme du mot
MAIL=$6 # adresse mail du destinataire
FORMAT=$7 #0 Texte brut 1 Tree-Tagger 2 Cordial 3 Latin
CORPUSNAME=$8
GAPMIN=$9
GAPMAX=${10}
NBITEMSETMIN=${11}
NBITEMSETMAX=${12}
MINSUP=${13}


DIR="PROC" # répertoire de travail (contenant tous les sous scripts)
PATTERNS="out$2" # nom du fichier de sortie
ARG="ARG$2" # nom du fichier contenant les choix utilisateurs (nom, verbe, etc.)
TMPDIR="out"


echo "start" > logs/logdebug.txt
echo $LANG >> logs/logdebug.txt
echo $CHOIX >> logs/logdebug.txt
echo $FORMAT >> logs/logdebug.txt
echo $TMPDIR >> logs/logdebug.txt
echo $CORPUS >> logs/logdebug.txt
echo $CORPUS_TAG >> logs/logdebug.txt
echo $GAPMIN >> logs/logdebug.txt
echo $GAPMAX >> logs/logdebug.txt
echo $NBITEMSETMIN >> logs/logdebug.txt
echo $NBITEMSETMAX >> logs/logdebug.txt
echo $MINSUP >> logs/logdebug.txt
echo $TOOL >> logs/logdebug.txt

# mise en base du traitement
# php shell.php "add" $$ $MAIL $CORPUS_TAG "${CORPUSNAME}" "${TOOL}" $GAPMIN $GAPMAX $NBITEMSETMIN $NBITEMSETMAX $MINSUP >> logs/errors.log

#creation dossier resultat
mkdir $TMPDIR/$CORPUS_TAG 2>>logs/logdebug.txt
cp binaire/$TOOL $TMPDIR/$CORPUS_TAG/$TOOL 2>>logs/logdebug.txt

touch $DIR/$ARG
shift 13 # fait un saut de treiz dans $@, qui contient les argmuments de exec.sh. On part du 14ème pour gérer la contrainte d'appartenance, dont on ne connait pas le nombre d'arguments, 
#le bash ne gérant pas le passage de tableau en argument. A la fin de la boucle, le fichier ARG contient les choix des utilisateurs pour l'appartenance.
for i in "$@"
do
	echo $i >> $DIR/$ARG
done


#Treetagger
cd $DIR
if [[ $FORMAT == 1 ]] #Treetag
then
	echo $CORPUS_TAG >> log1.txt
	cp $CORPUS ./$CORPUS_TAG
elif [[ $FORMAT == 2 ]] #Cordial
then
	perl -ne 'chomp ; s/PCTFORTE/SENT/g; s/([^\t]+)\t+([^\t]+)\t+([^\s]+)/$1\t$3\t$2\n/g; print ;' $CORPUS > ./$CORPUS_TAG
	#awk -F '\t' '{ sub(/PCTFORTE/, "<SENT>"); printf "%s", $1"\t"$3"\t"$2"\n" }' $CORPUS > ./$CORPUS_TAG
elif [[ $FORMAT == 0 ]] #Texte brute
then
	if [[ $LANG == 0 ]] # permet de géner le français ou l'anglais pour le lancement du treetagger
		then Tag/cmd/tree-tagger-english $CORPUS > $CORPUS_TAG 2>>../logs/logtreetager.txt
		else Tag/cmd/tree-tagger-french $CORPUS > $CORPUS_TAG 2>>../logs/logtreetager.txt
	fi
elif [[ $FORMAT == 3 ]] #Latin
then
	xlsx2csv -d tab -s 1 $CORPUS > temp_${CORPUS_TAG}
	perl -ne 'chomp; s/\.\t\.\t\./\.\tSENT\t\./g; s/([^\t]+)\t([^\t]+)\t([^\t]+).*/$1\t$2\t$3\n/g; print;' temp_${CORPUS_TAG} > $CORPUS_TAG
fi

cp $CORPUS_TAG ../$TMPDIR/$CORPUS_TAG/Treetag_data
./RewriteTag.pl $CORPUS_TAG $CHOIX # écrit le fichier d'entré de l'extracteur de motif, ainsi que le fichier des correspondances (mot <-> id). PROPRE AU TREETAGGER. ?placer dans la boucle??
./rewrite.pl Dmt4_$CORPUS_TAG # gérere un fichier d'entré 'propre' (sans doublon et avec itensets triés)

cp Dmt4_${CORPUS_TAG}ok ../$TMPDIR/$CORPUS_TAG/Dmt4_data



cd ../$TMPDIR/$CORPUS_TAG
echo -e "GAPMIN=$GAPMIN" > Load.ini
echo -e "GAPMAX=$GAPMAX" >> Load.ini
echo -e "NB_ITEMSET_MIN=$NBITEMSETMIN" >> Load.ini
echo -e "NB_ITEMSET_MAX=$NBITEMSETMAX" >> Load.ini
echo -e "MINSUP=$MINSUP" >> Load.ini
echo -e "CORPUS=Dmt4_data" >> Load.ini
# écriture dans le fichier ini de l'extracteur de motifs des id pour la contrainte d'appartenance
cd ../../PROC
./ListCat.pl Correspondance_$CORPUS_TAG $ARG $TMPDIR/$CORPUS_TAG $LANG # script gérant la contrainte d'appartenance
cd ../$TMPDIR/$CORPUS_TAG

./$TOOL > resultat_0 2>exec.log # exécution de l'extracteur

cd ../../$DIR
./Convert_ID_text.pl ../$TMPDIR/$CORPUS_TAG/resultat_0 Correspondance_$CORPUS_TAG > $PATTERNS.correspondance # écriture du fichier contrainte les motifs avec les mots correspondants


# écriture d'un message si le fichier de sortie est vide
if [ ! -s $PATTERNS.correspondance ]; then
    echo -e "Le fichier est vide." > $PATTERNS.correspondance
fi

mv $PATTERNS.correspondance ../$TMPDIR/$CORPUS_TAG/resultat_correspondance # sauvegarde du fichier de sortie
mv Correspondance_$CORPUS_TAG ../$TMPDIR/$CORPUS_TAG/correspondance # sauvegarde du fichier de sortie

rm -f *$CORPUS_TAG* # suppression des fichiers temporaires

rm ../$TMPDIR/$CORPUS_TAG/$TOOL
rm ../$TMPDIR/$CORPUS_TAG/Load.ini

# Not used
# rm ../$TMPDIR/$CORPUS_TAG/TIME

# envoi d'un mail à l'utilisateur
# php ../shell.php "mail" $CORPUS_TAG $MAIL >> ../logs/errors.log

# suppression du traitement en base
# php ../shell.php "remove" $$ >> ../logs/errors.log

#ajout de la génération du XML-TEI-URS
cd ../$TMPDIR/$CORPUS_TAG/
touch running
../../script_toXML.pl resultat_0 Dmt4_data $GAPMIN $GAPMAX $CORPUS 2>resu.log
rm running
