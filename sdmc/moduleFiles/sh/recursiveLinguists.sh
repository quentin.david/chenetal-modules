#!/bin/bash

nom=$1
gapmin=$2
gapmax=$3
nbitemsetmin=$4
nbitemsetmax=$5
minsup=$6
relatif=$7
thread=$8
corpus=$9
tool=${10}
nbresult=${11}
format=${12}
lang=${13}
choix=${14}
email=${15}
corpusname=${16}

PATTERNS="out$1"

# mise en base du traitement
php shell.php "add" $$ $email $nom "${corpusname}" "${tool}" $gapmin $gapmax $nbitemsetmin $nbitemsetmax $minsup >> logs/errors.log


cd PROC/

if [[ $format != 2 ]] 
	then
	if [[ $format == 1 ]]
		then cp $corpus ./$nom
	else 
		if [[ $lang == 0 ]] # permet de géner le français ou l'anglais pour le lancement du treetagger
			then Tag/cmd/tree-tagger-english $corpus > $nom 2> err
			else Tag/cmd/tree-tagger-french $corpus > $nom 2> err
		fi
	fi
else 
	perl -ne 'chomp ; s/PCTFORTE/SENT/g; s/([^\t]+)\t+([^\t]+)\t+([^\s]+)/$1\t$3\t$2\n/g; print ;' $corpus > ./$nom
    #awk -F '\t' '{ sub(/PCTFORTE/, "<SENT>"); print $1"\t"$3"\t"$2 }' $corpus > ./$nom
fi

cp $nom ../out/$nom/Treetag_data

./RewriteTag.pl $nom $choix # écrit le fichier d'entré de l'extracteur de motif, ainsi que le fichier des correspondances (mot <-> id). PROPRE AU TREETAGGER. ?placer dans la boucle??
./rewrite.pl Dmt4_$nom # gérere un fichier d'entré 'propre' (sans doublon et avec itemsets triés)


cd ../

mkdir out/$nom
cp PROC/Dmt4_${nom}ok out/$nom/Dmt4_data
cp binaire/$tool out/$nom/$tool
mv PROC/Correspondance_$nom out/$nom/correspondance # sauvegarde du fichier de sortie

perl sh/recursiveLinguists.pl $nom $gapmin $gapmax $nbitemsetmin $nbitemsetmax $minsup $relatif $thread Dmt4_data $tool $nbresult "${corpusname}" 2>> logs/recurLinguists.log

rm -f PROC/*$nom* # suppression des fichiers temporaires

# envoi d'un mail à l'utilisateur
php shell.php "mail" $nom $email >> logs/errors.log

# suppression du traitement en base
php shell.php "remove" $$ >> logs/errors.log

