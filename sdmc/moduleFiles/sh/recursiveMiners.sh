#!/bin/bash

nom=$1
gapmin=$2
gapmax=$3
nbitemsetmin=$4
nbitemsetmax=$5
minsup=$6
relatif=$7
thread=$8
corpus=$9
tool=${10}
nbresult=${11}
email=${12}
corpusname=${13}

# mise en base du traitement
php shell.php "add" $$ $email $nom "${corpusname}" "${tool}" $gapmin $gapmax $nbitemsetmin $nbitemsetmax $minsup >> logs/errors.log

mkdir out/$nom
cp $corpus out/$nom/Dmt4_data
cp binaire/$tool out/$nom/$tool

perl sh/recursiveMiners.pl $nom $gapmin $gapmax $nbitemsetmin $nbitemsetmax $minsup $relatif $thread Dmt4_data $tool $nbresult "${corpusname}" 2>> logs/recurMiners.log

# envoi d'un mail à l'utilisateur
php shell.php "mail" $nom $email >> logs/errors.log

# suppression du traitement en base
php shell.php "remove" $$ >> logs/errors.log

