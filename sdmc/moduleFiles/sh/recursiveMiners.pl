#!/usr/bin/perl -w

use strict;
use warnings;
use utf8;
use File::Copy qw(copy);

#fonction qui calcule le nombre d'occurence d'un motif de résultat récursif dans le corpus initial
sub nbligne_initial{
    my ($uid,$num,$corpus,$gapmin,$gapmax) = @_;
    my $resultatNull = 0;
    
    open (ENTREE, "resultat_$num") or die("Erreur à l'ouverture du fichier 'resultat_$num'");
    open (SORTIE, ">resultat_".$num."_ok") or die("Erreur à l'ouverture du fichier 'resultat_".$num."_ok'");

    while (<ENTREE>){
        chomp $_;
        #découpage de la ligne entre les motifs et leurs support
        my @Ligne = split (/\s\:/, $_);
        #découpage de la ligne en liste de mots entre chaque espace (liste des itemsets)
        my @Motif = split (/\s/, $Ligne[0]);
        my $regex = "";   
        foreach my $Motif (@Motif) {
            #Vérifie que l'itemset est bien de la forme {xx}, et suppression des {}
            if ($Motif =~ m/\{+\S+\}/){
                $Motif =~ s/\{//;
                $Motif =~ s/\}//;
                #découpage de l'itemset en items
                my @item = split(/,/, $Motif);  
                #s'il y a deux items (2 étant le maximum d'items possible)
                if(@item == 2){
                    $regex .= " $item[0],$item[1]( ([0-9]*,){0,1}[0-9]*(,[0-9]*){0,1}){$gapmin,$gapmax}";
                #s'il n'y en a qu'un (il ne peut pas y en avoir 0)
                }else{
                    $regex .= " ([0-9]*,){0,1}$item[0](,[0-9]*){0,1}( ([0-9]*,){0,1}[0-9]*(,[0-9]*){0,1}){$gapmin,$gapmax}";
                }  
            }
        }
        $regex .= " ";
        
        #on cherche les occurences du regex par seqId
        my $ligne = ".";
        my $cpt = 0;
        my $temp = 0;
        
        open (LECORPUS, "$corpus") or die("Erreur à l'ouverture du fichier '$corpus'");
        while (<LECORPUS>){
            chomp $_;
            #si la ligne est un nouveau seqId (nouvelle phrase)
            if($_ =~ m/seqId/){    
                #on ferme notre ligne qui contient la phrase
                $ligne .= " .";
                #si notre phrase contient le regex
                if($ligne =~ m/($regex)/){
                    #on incrémente le compteur d'occurences
                    $cpt++;
                }
                #on réinitialise la phrase et le compteur d'itemset
                $ligne = ". ";
                $temp = 0;
            #sinon c'est qu'on est en cours de construction de la phrase
            }else{
                #on découpe la ligne qui contient "x y"
                #x étant le numéro d'itemset, et y un de ses item
                my @LigneCourante = split (/\s/, $_);
                #si x est le même que la ligne précendente
                if($temp == $LigneCourante[0]){
                    #il sont du même itemset donc on les sépare d'une ","
                    $ligne .= ",$LigneCourante[1]";
                 #sinon ce sont deux itemsets différents
                }else{
                    #on les sépare d'un " "
                    $temp = $LigneCourante[0];
                    $ligne .= " $LigneCourante[1]";
                }
            }
        }
        #on affiche la ligne avec le nouveau nombre de résultat
        print SORTIE "$Ligne[0] : $cpt\n";
        close(LECORPUS) or die("Erreur à la fermeture");

    }

    close(SORTIE) or die("Erreur à la fermeture");
    close(ENTREE) or die("Erreur à la fermeture");
}

#fonction pour transformer la sortie du binaire en entree
#permet également en passant de calculer le nombre de ligne, et donc de motifs, du fichier résultat
sub rename_fichier{
    my ($uid,$num) = @_;
    #on récupère le numéro du passage précedent, et on l'incrémente pour créer un nouveu fichier entrée
    my $numin = $num +1;
    #booleen pour gérer le retour à la ligne sur les nouvelles phrases en évitant la ligne vide 
    #en fin de fichier qui n'est pas apprécié par speedbide
    my $first = 1;
    #on ouvre le fichier résulat en lecture, et notre nouveau fichier d'entrée en écriture
	open (Result, "resultat_$num") or die("Erreur à l'ouverture du fichier 'resultat_$num'");
	open (Out, ">entree_$numin") or die("Erreur à l'ouverture du fichier 'entree_$numin.in'");
	binmode(Out, ':utf8');
    #compteur de phrases (seqId) et donc du nombre de motifs
	my $cptLine = 1;
	#pour chaque ligne du fichier résultat (pour chaque motifs d'itemsets)
    while (<Result>){
        #suppression des caractères d'échappement
	  	chomp $_;
        if($first){
            print Out "seqId $cptLine";
        }else{
            print Out "\nseqId $cptLine";
        } 
        #découpage de la ligne en liste de mots entre chaque espace (liste des itemsets)
	  	my @Mots = split (/\s/, $_);
	  	my $cptWord = 1;
        #pour chaque itemset
	  	foreach my $mot (@Mots) {
            #regex permettant de vérifier que l'itemset est bien de la forme {xx}, et suppression des {}
	  		if ($mot =~ m/\{+\S+\}/){
				$mot =~ s/\{//;
				$mot =~ s/\}//;
				#cas d'un resultat de type {xx,yy}
				if($mot =~ m/\S+,+\S/){
                    #on découpe l'itemset en item
					my @temp = split (/,/, $mot);
                    #pour chaque item on l'écris sur une nouvelle ligne avec son numéro d'itemset
					foreach my $temp (@temp){
						print Out "\n$cptWord $temp" ;
					}
					$cptWord ++ ;
				# cas d'un resultat normal {xx}
				} else {
                    #on écrit l'itemset sur une nouvelle ligne
		  			print Out "\n$cptWord $mot";
		  			$cptWord ++ ;
				}
	  		}
	  	}	
	  	$cptLine ++;
        $first = 0;
	}
    #on ferme les fichiers
	close(Result) or die("Erreur à la fermeture");
	close(Out);
    # on retourne le nombre de motifs
    return $cptLine-1;
}

#Fonction de Mr Nicolas Bechet : Convert_ID_text.pl
sub correspondance{
    my ($uid,$num,$corresp) = @_;
    open (Correspondance, $corresp) or return 0;
    if($num != 0){
        open (Result, "resultat_".$num."_ok") or die("Erreur à l'ouverture du fichier 'resultat_".$num."_ok' pour correspondance");
    }else{
        open (Result, "resultat_$num") or die("Erreur à l'ouverture du fichier 'resultat_$num' pour correspondance");
    }
	open (Out, "> resultat_correspondance") or die("Erreur à l'ouverture du fichier 'resultat_correspondance'");
	print "Creation fichier correspondance : " ;
	binmode(Out, ':utf8');
	
    #création du tableau de correspondance entre nombre et mot
	my %Correspondance;
	foreach (<Correspondance>){
		chomp $_;
		my @Correspondance = split (/\t/, $_);
		$Correspondance{$Correspondance[1]} = $Correspondance[0];
	}
	
    #pour chaque ligne du fichier resultat
	foreach (<Result>){
        #suppression des caractères d'échappement
		chomp $_;
        #découpage de la ligne entre les motifs et le nombre de résultat
		my @Result = split (/:/, $_);
        #découpage de la ligne en liste de mots entre chaque espace (liste des itemsets)
		my @Itemset = split (/\s/, $Result[0]);
        #pour chaque itemset
		foreach my $Itemset (@Itemset){
            #on supprime les crochets
			$Itemset =~ s/\{|\}//g;
            #découpage par item
			my @item = split(/,/, $Itemset);
            #on affiche la correspondance de l'item
			print Out '{';
			print Out $Correspondance{$item[0]};
            #si l'itemset a d'autres item, on les affiche également
			for (my $i = 1 ; $i <= $#item ; $i++){
				print Out ','.$Correspondance{$item[$i]};
			}
			print Out '} ';
		}
        #on réaffiche le nombre de résultat de la ligne
		print Out ":".$Result[1]."\n";
	}
    print "done \n" ;	
    close(Result);
    close(Correspondance);
    close(Out);
    return 1;
}

sub modif_load{
    my ($uid,$gapmin,$gapmax,$nbitemmin,$nbitemmax,$minsup,$thread,$corpus,$assoc) = @_;
    open (Load, ">Load.ini") or die("Erreur à l'ouverture du fichier 'Load.ini'");
    print(Load "GAPMIN=$gapmin\n");
    print(Load "GAPMAX=$gapmax\n");
    print(Load "NB_ITEMSET_MIN=$nbitemmin\n");
    print(Load "NB_ITEMSET_MAX=$nbitemmax\n");
    print(Load "MINSUP=$minsup\n");
    print(Load "THREAD=$thread\n");
    print(Load "CORPUS=$corpus\n");
    #if ($assoc eq 1){
    #    print(Load "IN=2,1\n");
    #    print(Load "OR=6,10,19,32,46,113,15,21\n");
    #    #print(Load "ASSOCIATION=0\n");
    #}
    close(Load);
}

sub relatif{
    my ($file,$minsup) = @_;
    
    open(F, $file) or die("Erreur à l'ouverture du fichier '$file'");
    my $i = 0;
    while (<F>){
        chomp $_;
        if ($_ =~ m/seqId/){
            $i ++;
        }
    }
    close(F);
    
    #print("Récupération de $i seqId\n");
	return int($i*($minsup/100));
}

my $minsup_rel = 99;
my ($uid,$gapmin,$gapmax,$nbitemmin,$nbitemmax,$minsup,$relatif,$thread,$corpus,$binaire,$cdArret,$corpusname) = @ARGV;
my @LigneResultat;
my $tpsFin;
my $tpsExec;
my $tpsDebut;

my $assoc = 0;
my $nbrec = 0;
#suppression du % au cas ou
$minsup =~ s/\%//;

#my $corpusAff = $corpus;
#$corpusAff =~ s/corpus\///;
#my $texte = $corpus;
#$texte =~ s/corpus\/Dmt4_//;
#$texte =~ s/ok//;
my $correspondance = "correspondance" ;
#my $cheminCorrespondance = "" ;
#my $cheminTexte = "../../corpus/Texte/" ;
#$corpus = "../../$corpus";

chdir("out/$uid/");

open(START, ">START" ) or die "cannot create START : $!\n";
close(START);
open(my $oldout, ">&STDOUT") or die "Can't dup STDOUT: $!";
open(STDOUT, ">stdout.log" ) or die "cannot redirect stdout output in log file : $!\n"; $|++;
open(STDERR, ">stderr.log" ) or die "cannot redirect stderr output in log file : $!\n";
binmode(STDOUT, ':utf8');
binmode(STDERR, ':utf8');

print("ID de l'itération : \n$uid\n\n");
#if ($corpus eq "../../corpus/Dmt4_test.txtok"){
#    $assoc = 1;
#}

#Creation du rapport latex
open (Latex, ">rapport.tex") or die("Erreur à l'ouverture du fichier 'rapport.tex'");

print Latex "\\documentclass[a4paper,11pt]{article}\n";
print Latex "\\usepackage[T1]{fontenc}\n";
print Latex "\\usepackage{verbatim}\n";
print Latex "\\usepackage{listings}\n";
print Latex "\\lstset{ %\n";
print Latex "   basicstyle=\\footnotesize\\ttfamily,\n";        # the size of the fonts that are used for the code
print Latex "   breakatwhitespace=true,\n";         # sets if automatic breaks should only happen at whitespace
print Latex "   breaklines=true,\n";                 # sets automatic line breaking
print Latex "   frame=single,\n";                    # adds a frame around the code
print Latex "   showspaces=false,\n";                # show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
print Latex "   xleftmargin=-2cm,\n";            
print Latex "   xrightmargin=-2cm,\n";
print Latex "   showstringspaces=false\n";           # underline spaces within strings only
print Latex "\}\n";       
print Latex "\\usepackage{pstricks}\n";
print Latex "\\usepackage{pst-plot}\n";
print Latex "\\usepackage[english,francais]{babel}\n";
print Latex "\\usepackage{hyperref}\n";
print Latex "\\addtolength{\\voffset}{-2cm} \n";
print Latex "\\addtolength{\\textheight}{4cm} \n";
print Latex "\\title{Rapport DataMining $uid}\n";


print Latex "\\begin{document}\n";
print Latex "\\maketitle\n";
print Latex "\\tableofcontents\n";
print Latex "\\newpage\n";
print Latex "\\section{Parametres}\n";

print Latex "CORPUS=\\verb\?$corpusname\? \\newline ";
print "CORPUS=$corpusname \n";
if($binaire eq "bidespantree_v2") {
    print Latex "BINAIRE= bidespantree\\_v2 (fermes) \\newline ";
    print "BINAIRE= bidespantree_v2 (fermes) \n";
}
if($binaire eq "gap_bide") {
    print Latex "BINAIRE= gap\\_bide (fermes) \\newline ";
    print "BINAIRE= gap_bide (fermes) \n";
}
if($binaire eq "prefix_constraint") {
    print Latex "BINAIRE= prefix\\_constraint (frequents) \\newline ";
    print "BINAIRE= prefix_constraint (frequents) \n";
}
print Latex "GAPMIN=$gapmin \\newline ";
print "GAPMIN=$gapmin \n";
print Latex "GAPMAX=$gapmax \\newline ";
print "GAPMAX=$gapmax \n";
print Latex "NB\\_ITEMSET\\_MIN=$nbitemmin \\newline ";
print "NB_ITEMSET_MIN=$nbitemmin \n";
print Latex "NB\\_ITEMSET\\_MAX=$nbitemmax \\newline ";
print "NB_ITEMSET_MAX=$nbitemmax \n";
if ($relatif){
    print Latex "MINSUP=$minsup\\% \\newline ";
    print "MINSUP=$minsup % \n\n";
}else{
    print Latex "MINSUP=$minsup \\newline ";
    print "MINSUP=$minsup \n\n";
}

print Latex "\\section{Resultat source}\n";
#boucle d'appel du binaire
#début du programme principal
while(1){
    #Calcul de la valeur relative de minsup pour le prochain appel
    #et creation du fichier Load.ini
    if ($relatif){
        if ($nbrec == 0){
            $minsup_rel = relatif($corpus,$minsup);
            modif_load($uid,$gapmin,$gapmax,$nbitemmin,$nbitemmax,$minsup_rel,$thread,$corpus,$assoc);
            if ($minsup_rel <= 1){
                $nbrec = $nbrec - 1;
                last;
            }
        }else{
            $minsup_rel = relatif("entree_$nbrec",$minsup);
            modif_load($uid,$gapmin,$gapmax,$nbitemmin,$nbitemmax,$minsup_rel,$thread,"entree_$nbrec",$assoc);
            if ($minsup_rel <= 1){
                $nbrec = $nbrec - 1;
                last;
            }
        }
    }else{
        if ($nbrec == 0){
            modif_load($uid,$gapmin,$gapmax,$nbitemmin,$nbitemmax,$minsup,$thread,$corpus,$assoc);
            if ($minsup_rel <= 1){
                $nbrec = $nbrec - 1;
                last;
            }
        }else{
            modif_load($uid,$gapmin,$gapmax,$nbitemmin,$nbitemmax,$minsup,$thread,"entree_$nbrec",$assoc);
            if ($minsup_rel <= 1){
                $nbrec = $nbrec - 1;
                last;
            }
        }
    }

    if ($nbrec == 0){
        print Latex "\\subsection{Passage initial}\n";
    }else{
        print Latex "\\subsection{Passage Recursif $nbrec}\n";
    }
    
    print Latex "\\subsubsection{Used params}\n";
    if ($relatif){ 
        print Latex "MINSUP=$minsup_rel \\newline ";
    }else{
        print Latex "MINSUP=$minsup \\newline ";
    }

    #1ere utilisation du binaire
    print "Exécution n°$nbrec : \n" ;
    #on récupere le temps au lancement
    use Time::HiRes qw(time);
    $tpsDebut = time;
    
    #on éxécute
    qx{./$binaire > resultat_$nbrec};
    
    #on récupere le temps à la fin
    use Time::HiRes qw(time);
    $tpsFin = time;
    $tpsExec = $tpsFin - $tpsDebut;
    
    printf("Durée du calcul : %.3f secondes\n", $tpsExec);
    #Transformation de la sortie en entrée
	$LigneResultat[$nbrec] = rename_fichier($uid,$nbrec);
    print "Nombre de motifs : $LigneResultat[$nbrec]\n\n" ;
    
    
    
    
    
    print Latex "\\subsubsection{Results}\n";
    
    print Latex "$LigneResultat[$nbrec] motifs trouvés. \\newline ";
    
    if($LigneResultat[$nbrec] != 0){
        printf Latex ("\nDurée du calcul : %.3f secondes \\newline ", $tpsExec);
        print Latex "\\href{https://sdmc.greyc.fr/out/$uid/resultat\\_$nbrec}{Lien vers fichier résultat} \\newline ";
        
        
    }
    last if ($LigneResultat[$nbrec] <= $cdArret);
    $nbrec = $nbrec + 1;
}

if ($LigneResultat[$nbrec] != 0){
    if($nbrec != 0){
        print "Calcul du support initial\n\n" ;
        nbligne_initial($uid,$nbrec,$corpus,$gapmin,$gapmax);
        print Latex "\\href{https://sdmc.greyc.fr/out/$uid/resultat\\_$nbrec\\_ok}{Lien vers fichier résultat avec support du Corpus initial } \\newline ";
    }
    print Latex "\\newpage\n";
    print Latex "\\subsubsection{Details}\n";

    if($nbrec != 0){
        open (RESULTAT, "resultat_".$nbrec."_ok") or die("Erreur à l'ouverture du fichier 'resultat_".$nbrec."_ok'");
    }else{
        open (RESULTAT, "resultat_$nbrec") or die("Erreur à l'ouverture du fichier 'resultat_nbrec'");
    }

    print Latex "\\begin{lstlisting}\n";
    while (<RESULTAT>){
        print Latex "$_";
    }
    print Latex "\\end{lstlisting}\n";
    close(RESULTAT);

}

print Latex "\\end{document}\n";

close(Latex);

print "Génération du rapport : ";
#system("chmod 755 rapport.tex"); # à déco
#system("pdflatex rapport.tex >/dev/null 2>/dev/null"); # à déco
#system("pdflatex rapport.tex >/dev/null 2>/dev/null"); # à déco
system("pdflatex rapport.tex");
system("pdflatex rapport.tex");


#system("latex rapport.tex >/dev/null 2>/dev/null");
#system("latex rapport.tex >/dev/null 2>/dev/null");
#system("dvips rapport.dvi >/dev/null 2>/dev/null");
#system("ps2pdf rapport.ps >/dev/null 2>/dev/null");
print "done\n";

close(STDOUT);
close(STDERR);
unlink("$binaire");
unlink("Load.ini");
unlink("START");
unlink("TIME");
unlink("rapport.aux");
unlink("rapport.log");
unlink("rapport.out");
unlink("rapport.tex");
unlink("rapport.toc");

#open(STDOUT, ">&", $oldout) or die "Can't dup STDOUT: $!";
#print $LigneResultat[$nbrec];

