#!/bin/bash
CORPUS=$1 # nom du corpus donné par l'utilisateur
CORPUS_TAG=$2 # nom généré aléatoirement
TOOL=$3 # répertoire de l'extracteur de motif
MAIL=$4 # adresse mail du destinataire
CORPUSNAME=$5
GAPMIN=$6
GAPMAX=$7
NBITEMSETMIN=$8
NBITEMSETMAX=$9
MINSUP=${10}

ARG="ARG$2" # nom du fichier contenant les choix utilisateurs (nom, verbe, etc.)
PATTERNS="out$2" # nom du fichier de sortie
DIR="PROC/" # répertoire de travail (contenant tous les sous scripts)
TMPDIR="out"

# mise en base du traitement
php shell.php "add" $$ $MAIL $CORPUS_TAG "${CORPUSNAME}" "${TOOL}" $GAPMIN $GAPMAX $NBITEMSETMIN $NBITEMSETMAX $MINSUP >> logs/errors.log

mkdir $TMPDIR/$CORPUS_TAG
cp binaire/$TOOL $TMPDIR/$CORPUS_TAG/$TOOL

cd $TMPDIR/$CORPUS_TAG
echo -e "GAPMIN=$GAPMIN" > Load.ini
echo -e "GAPMAX=$GAPMAX" >> Load.ini
echo -e "NB_ITEMSET_MIN=$NBITEMSETMIN" >> Load.ini
echo -e "NB_ITEMSET_MAX=$NBITEMSETMAX" >> Load.ini
echo -e "MINSUP=$MINSUP" >> Load.ini
echo -e "CORPUS=$CORPUS" >> Load.ini

./$TOOL > resultat_0 2> exec.log # exécution de l'extracteur

# écriture d'un message si le fichier de sortie est vide
if [ ! -s resultat_0 ]; then
    echo -e "Le fichier est vide." > resultat_0
fi

rm $TOOL
rm Load.ini
rm TIME
#rm -f *$CORPUS_TAG* # suppression des fichiers temporaires

# envoi d'un mail à l'utilisateur
php ../../shell.php "mail" $CORPUS_TAG $MAIL >> ../../logs/errors.log

# suppression du traitement en base
php ../../shell.php "remove" $$ >> ../../logs/errors.log
