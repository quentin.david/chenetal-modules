<?php

require("web/config/config.php");

$page = (isset($_GET["p"])) ? $_GET["p"] : "welcome";

// on récupère les infos sur l'utilisateur courant
if(isset($_SESSION["email"])) {
	$currentUser = $userDB->getUser($_SESSION["email"]);
}

try {
	switch ($page) {

		case "welcome" :
			$title = "Bienvenue";
			unset($_SESSION["error"]);
			$content = Template::render("welcome");
		break;

	    case "login" :
			$title = "S'identifier";
			$category = (isset($_GET["category"])) ? $_GET["category"] : "none";
			$content = Template::render("loginForm", array("category" => $category));
		break;

		case "signIn" :
			$title = "Créer un compte";
			$content = Template::render("addUser", array("action" => "index.php?p=signIn"));
			if(isset($_POST["email"])) {
			    if($userDB->isUser($_POST["email"])){
				    $title = "Erreur dans la prise en compte";
			        $content = "Cet email est déjà enregistré dans notre base de données.";
                }else{
                    $userDB->addUser($_POST);
                    $user = $userDB->getUser($_POST["email"]);

			        $title = "Demande prise en compte";
			        $content = "Nous avons bien pris en compte votre demande de compte. Un mail vous sera envoyé dès l'activation de votre compte par un administrateur.";

			        foreach ($userDB->getAdmins() as $admin) {
                        $mailer->accountDemand($user,$admin);
                    }
                }
			}
		break;

	    case "doLogin" :
			$category = (isset($_GET["category"])) ? $_GET["category"] : "none";
			if ($userDB->login($_POST["email"], $_POST["password"])) {
				$user = $userDB->getUser($_SESSION["email"]);
				$category = ($category == "none") ? $user->getCategory() : $category;
				header("Location: user.php?p=form&category=" . $category);
			} else {
				header("Location: index.php?p=login&category=" . $category);
			}
		break;

	    case "logout" :
			session_destroy();
			header("Location: index.php");
		break;

		case "forbidden" :
			$title = $content = "Accès interdit.";
		break;

	    default :
			$title = "Page non trouvée";
			$content = "Erreur 404";
		break;
	} // end switch
} // end try
catch(Exception $e) {
	$title = "Erreur";
	$content = $e->getMessage();
}
if(!defined("AJAX")) {
	include("web/ui/pages/page.php");
} else {
	echo $content;
}
?>
