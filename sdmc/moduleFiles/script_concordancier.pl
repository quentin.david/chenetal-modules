#!/usr/bin/perl -w

#use strict;
# Problem with perl locales in the stderr....
# use warnings;
no warnings 'all';
use utf8;

my ($uid,$file,$nl,$corpus,$gapmin,$gapmax,$decal) = @ARGV;

chdir("out/$uid/");
open (IN, $file) or die("Erreur à l'ouverture du fichier '$file'");
open (OUT, ">concordancier") or die("Erreur à l'ouverture du fichier 'concordancier'");
open (Correspondance, "correspondance") or die("Erreur à l'ouverture du fichier 'correspondance'");
binmode(STDOUT, ':utf8');
binmode(STDERR, ':utf8');

my %Correspondance;
while (<Correspondance>){
    chomp $_;
    my @Correspondance = split (/\t/, $_);
    $Correspondance{$Correspondance[1]} = $Correspondance[0];
}
close(Correspondance) or die("Erreur à la fermeture");

my $cpt_motif = 0;

while (<IN>){
    if ($cpt_motif eq $nl){
        chomp $_;
        
        my @Res = split (/\:/, $_);
        my @Motif = split (/\s/, $Res[0]);
		
		my $regex = "";
        
		for (my $i = 0 ; $i <= $#Motif-1 ; $i++){        	
            if ($Motif[$i] =~ m/\{+\S+\}/){
                $Motif[$i] =~ s/\{//;
                $Motif[$i] =~ s/\}//;
				
                my @item = split(/,/, $Motif[$i]);    
                if(@item == 2){
                    $regex .= " ($item[0],$item[1])(?: (?:[0-9]*,){0,1}[0-9]*(?:,[0-9]*){0,1}){$gapmin,$gapmax}";
                }else{
                    $regex .= " (?:[0-9]*,){0,1}($item[0])(?:,[0-9]*){0,1}(?: (?:[0-9]*,){0,1}[0-9]*(?:,[0-9]*){0,1}){$gapmin,$gapmax}";
                }        
            }
        }
		if ($Motif[$#Motif] =~ m/\{+\S+\}/){
			$Motif[$#Motif] =~ s/\{//;
			$Motif[$#Motif] =~ s/\}//;

			my @item = split(/,/, $Motif[$#Motif]);    
			if(@item == 2){
				$regex .= " ($item[0],$item[1])(?: (?:[0-9]*,){0,1}[0-9]*(?:,[0-9]*){0,1}){$gapmin,$gapmax} ";
			}else{
				$regex .= " (?:[0-9]*,){0,1}($item[0])(?:,[0-9]*){0,1} ";
			}        
		}
        
        my $ligne = "";
        my $temp = 0;
		my $cpt_ligne = -1;
        
        open (CORPUS, $corpus) or die("Erreur à l'ouverture du fichier '$corpus'");
        while (<CORPUS>){
            chomp $_;
            
            if($_ =~ m/seqId/){
				$cpt_ligne++;
                $ligne .= " ";
                if($ligne =~ m/$regex/p){
					
					my $lignetag = "";
					$lignetag .= substr( $ligne, 1, $-[1]-1 );
					$lignetag .= "<b>".substr( $ligne, $-[1], $+[1]-$-[1] )."</b>";
					foreach my $exp (2..$#-) {
						$lignetag .= substr( $ligne, $+[$exp-1], $-[$exp]-$+[$exp-1] );
						$lignetag .= "<b>".substr( $ligne, $-[$exp], $+[$exp]-$-[$exp] )."</b>";
					}
					$lignetag .= " ".${^POSTMATCH};					
					$lignetag =~ s/([0-9]+)/$Correspondance{$1}/g;

                    print OUT "$cpt_ligne\t ".$lignetag."\n";                
                }
				
                $ligne = "";
                $temp = 0;
            }else{
                my @LigneCourante = split (/\s/, $_);
                if($temp == $LigneCourante[0]){
                    $ligne .= ",$LigneCourante[1]";
                }else{
                    $temp = $LigneCourante[0];
                    $ligne .= " $LigneCourante[1]";
                }
            }
        }
        
		#dernière ligne, on doit refaire une analyse.
		$cpt_ligne++;
		$ligne .= " ";
		if($ligne =~ m/$regex/p){
			my $lignetag = "";
			$lignetag .= substr( $ligne, 1, $-[1]-1 );
			$lignetag .= "<b>".substr( $ligne, $-[1], $+[1]-$-[1] )."</b>";
			foreach my $exp (2..$#-) {
				$lignetag .= substr( $ligne, $+[$exp-1], $-[$exp]-$+[$exp-1] );
				$lignetag .= "<b>".substr( $ligne, $-[$exp], $+[$exp]-$-[$exp] )."</b>";
			}
			$lignetag .= " ".${^POSTMATCH};
			$lignetag =~ s/([0-9]+)/$Correspondance{$1}/g;
			
			print OUT "$cpt_ligne\t ".$lignetag."\n";            
		}		
        close(CORPUS) or die("Erreur à la fermeture");
        exit;
    }
    $cpt_motif ++;
}

close(OUT) or die("Erreur à la fermeture");
close(IN) or die("Erreur à la fermeture");
