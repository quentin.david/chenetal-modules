#!/usr/bin/perl -w

use strict;
use warnings;
use utf8;

my ($uid,$file,$nl,$corpus,$gapmin,$gapmax,$decal) = @ARGV;

chdir("out/$uid/");
open (IN, $file) or die("Erreur à l'ouverture du fichier '$file'");
open (OUT, ">concordancier") or die("Erreur à l'ouverture du fichier 'concordancier'");
open (Correspondance, "correspondance") or die("Erreur à l'ouverture du fichier 'correspondance'");
binmode(STDOUT, ':utf8');
binmode(STDERR, ':utf8');

my %Correspondance;
while (<Correspondance>){
    chomp $_;
    my @Correspondance = split (/\t/, $_);
    $Correspondance{$Correspondance[1]} = $Correspondance[0];
}
close(Correspondance) or die("Erreur à la fermeture");

my $cptLine = 0;

while (<IN>){
    if ($cptLine == $nl){
        chomp $_;
        
        my @Res = split (/\:/, $_);
        my @Motif = split (/\s/, $Res[0]);
        my @MotifCorrespondance;
        my $regex = "( ([0-9]*,){0,1}[0-9]*(,[0-9]*){0,1}){0,999}";
        my $first = 1;
        my $nbItem = 0;
        my $titre;
        
        foreach my $Motif (@Motif) {
            if ($Motif =~ m/\{+\S+\}/){
                $Motif =~ s/\{//;
                $Motif =~ s/\}//;

                my @item = split(/,/, $Motif);    
                if(@item == 2){
                    $regex .= " $item[0],$item[1]( ([0-9]*,){0,1}[0-9]*(,[0-9]*){0,1}){$gapmin,$gapmax}";
                    push (@MotifCorrespondance, "".$Correspondance{$item[0]}.",".$Correspondance{$item[1]}."");
                }else{
                    $regex .= " ([0-9]*,){0,1}$item[0](,[0-9]*){0,1}( ([0-9]*,){0,1}[0-9]*(,[0-9]*){0,1}){$gapmin,$gapmax}";
                    push (@MotifCorrespondance, $Correspondance{$item[0]});
                }        
            }
        }
        
        #print "Occurences de : @MotifCorrespondance \n\n"; # to comment
        
        $regex .= "( ([0-9]*,){0,1}[0-9]*(,[0-9]*){0,1}){0,999} ";
        
        my $ligne = "";
        my $aff = "";
        my $affOk = "";
        my $cpt = 0;
        my $temp = 0;
		my $cpt_ligne = -1;
        
        open (CORPUS, $corpus) or die("Erreur à l'ouverture du fichier '$corpus'");
        while (<CORPUS>){
            chomp $_;
            
            if($_ =~ m/seqId/){
				$cpt_ligne++;
                $ligne .= " ";
                if($ligne =~ m/($regex)/){
                    my @Itemset = split (/\s/, $1);
                    foreach my $Itemset (@Itemset){
                        if(not($Itemset eq "")){
                        
                            my @item = split(/,/, $Itemset);
                            if(@item == 2){
                                $aff .= $Correspondance{$item[0]}.','.$Correspondance{$item[1]}.' ';
                            }else{
                                $aff .= $Correspondance{$Itemset}.' ';
                            } 
                        }
                    }
                    $aff .= "\n";               
                    $cpt++;

                    #on cherche et met en évidence les items de la fouille récursive                
                    my @elements = split (/\s/, $aff);
                    my $cptMotif = 0;
                    my $size = $#elements - 999;
                    
                    for (my $i = 0 ; $i <= $#elements ; $i++){
                        # if($i < 4){
                            # $affOk .= $elements[$i]." ";
                        # }
                        # if($i == 4){
                            # $affOk .= $elements[$i]." | ";
                        # }
                        # if($i > 4 && $i <= $size){                        
                            if($cptMotif <= $#MotifCorrespondance){
                                if($MotifCorrespondance[$cptMotif] eq $elements[$i]){
                                    $cptMotif++;
                                    $affOk .= "<b>".$elements[$i]."</b> ";
                                }else{
                                    my @element = split(/,/, $elements[$i]);
                                    if(@element == 2){
                                        if($MotifCorrespondance[$cptMotif] eq $element[0]){
                                            $cptMotif++;
                                            $affOk .= "<b>".$element[0]."</b>,".$element[1]." ";
                                        }elsif($MotifCorrespondance[$cptMotif] eq $element[1]){
                                            $cptMotif++;
                                            $affOk .= $element[0].",<b>".$element[1]."</b> ";
                                        }else{
                                            $affOk .= $elements[$i]." ";
                                        } 
                                    }else{
                                        $affOk .= $elements[$i]." ";
                                    } 
                                
                                }
                            }else{
                                $affOk .= $elements[$i]." ";
                            }
                        # }
                        # if($i > $size){
                            # $affOk .= $elements[$i]." ";
                        # }
                    }
                    
                    print OUT "$cpt_ligne\t ".$affOk."\n";
                    $affOk = "";
                
                }
                
                
                $aff = "";
                $ligne = "";
                $temp = 0;
            }else{
                my @LigneCourante = split (/\s/, $_);
                if($temp == $LigneCourante[0]){
                    $ligne .= ",$LigneCourante[1]";
                }else{
                    $temp = $LigneCourante[0];
                    $ligne .= " $LigneCourante[1]";
                }
            }
        }
        
		#dernière ligne, on doit refaire une analyse.
		$cpt_ligne++;
		$ligne .= " ";
		if($ligne =~ m/($regex)/){
			my @Itemset = split (/\s/, $1);
			foreach my $Itemset (@Itemset){
				if(not($Itemset eq "")){
				
					my @item = split(/,/, $Itemset);
					if(@item == 2){
						$aff .= $Correspondance{$item[0]}.','.$Correspondance{$item[1]}.' ';
					}else{
						$aff .= $Correspondance{$Itemset}.' ';
					} 
				}
			}
			$aff .= "\n";               
			$cpt++;

			#on cherche et met en évidence les items de la fouille récursive                
			my @elements = split (/\s/, $aff);
			my $cptMotif = 0;
			my $size = $#elements - 999;
			
			for (my $i = 0 ; $i <= $#elements ; $i++){                    
					if($cptMotif <= $#MotifCorrespondance){
						if($MotifCorrespondance[$cptMotif] eq $elements[$i]){
							$cptMotif++;
							$affOk .= "<b>".$elements[$i]."</b> ";
						}else{
							my @element = split(/,/, $elements[$i]);
							if(@element == 2){
								if($MotifCorrespondance[$cptMotif] eq $element[0]){
									$cptMotif++;
									$affOk .= "<b>".$element[0]."</b>,".$element[1]." ";
								}elsif($MotifCorrespondance[$cptMotif] eq $element[1]){
									$cptMotif++;
									$affOk .= $element[0].",<b>".$element[1]."</b> ";
								}else{
									$affOk .= $elements[$i]." ";
								} 
							}else{
								$affOk .= $elements[$i]." ";
							} 
						
						}
					}else{
						$affOk .= $elements[$i]." ";
					}
			}
			
			print OUT "$cpt_ligne\t ".$affOk."\n";
			$affOk = "";
		
		}
		$aff = "";
		$ligne = "";
		$temp = 0;
		
        close(CORPUS) or die("Erreur à la fermeture");
        exit;
    }
    $cptLine ++;
}

close(OUT) or die("Erreur à la fermeture");
close(IN) or die("Erreur à la fermeture");
